@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;"><h3>REGISTRATION</h3></div>

                <div class="panel-body">
                    <form class="form-horizontal">
              <div class="form-group">
                <label for="property_name" class="col-sm-4 control-label">Property Name</label>
                <div class="col-sm-8">
                </div>
              </div>

              <div class="form-group">
                <label for="atoll" class="col-sm-4 control-label">Atoll</label>
                <div class="col-sm-2">
                  
                </div>
                <label for="island" class="col-sm-1 control-label">Island</label>
                <div class="col-sm-5">
                  
                </div>
              </div>

              <div class="form-group">
                <label for="distance_from_airport" class="col-sm-4 control-label">Distance from Airport</label>
                <div class="col-sm-8">
                </div>
              </div>

              <div class="form-group">
                <label for="no_of_rooms" class="col-sm-4 control-label">No. of Rooms</label>
                <div class="col-sm-8">
                </div>
              </div> 

              <div class="form-group">
                <label for="about" class="col-sm-4 control-label">About the Property</label>
                <div class="col-sm-8">
                </div>
              </div> 

              <div class="form-group">
                <label for="contact_number" class="col-sm-4 control-label">Contact Number</label>
                <div class="col-sm-8">
                </div>
              </div> 

              <div class="form-group">
                <label for="fax_number" class="col-sm-4 control-label">Fax Number</label>
                <div class="col-sm-8">
                </div>
              </div>    

              <div class="form-group">
                <label for="facebook" class="col-sm-4 control-label">Facebook</label>
                <div class="col-sm-8">
                </div>
              </div>   

              <div class="form-group">
                <label for="email" class="col-sm-4 control-label">Email Address</label>
                <div class="col-sm-8">
                </div>
              </div>    

              <div class="form-group">
                <label for="website" class="col-sm-4 control-label">Website</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="website" placeholder="Website" v-model="dataModel.website">
                </div>
              </div>

            </form>
                </div>

                <div class="panel-footer" style="text-align: right;">
                    <button type="button" class="btn btn-primary btn-flat">Submit</button>
                    <button type="button" class="btn btn-default btn-flat">Reset</button>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>

new Vue ({
    el: '#content',
    data: {
        dataEntrySuccessMessage: '',
        dataEntryErrorMessage: '',
        pageLoadErrorMessage: '',
        islands: [],
        dataModel: {
            property_name: '',
            atoll: 'A',
            island: '',
            distance_from_airport: '',
            no_of_rooms: '',
            about: '',
            contact_number: '',
            fax_number: '',
            facebook: '',
            email: '',
            website: ''
        }
    },
    methods: {
        queryIslands (event, letter) {
        if (event) {
          event.preventDefault()
        }

        let thisComponent = this

        console.log(thisComponent.dataModel.atoll)

        axios.get('/fetch-islands/' + thisComponent.dataModel.atoll)
        .then(function (response) {
          thisComponent.islands = response.data.data
        })
        .catch(function (error) {
          if (error.response) {
            if (error.response.data && error.response.data.errors) {
              thisComponent.pageLoadErrorMessage = error.response.data.errors[0].detail
            }
          } else {
            thisComponent.pageLoadErrorMessage = 'Unknown error.'
          }
          console.log(error)
        })
      },
      registerProperty () {
        let data = this.dataModel

        let thisComponent = this

        axios.post('/register', data)
          .then(function (response) {

            thisComponent.dataEntrySuccessMessage = 'Your entry for ' + data.property_name + 'has been submitted successfully.'
            // thisComponent.employees.unshift(response.data)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.dataEntryErrorMessage = error.response.data.errors[0].detail
              }
            } else {
              thisComponent.dataEntryErrorMessage = 'Unknown error.'
            }
          })
      },
      confirmDeletePhoto (e) {
        this.deleteModel.obj = e
      },
      deletePhoto (e) {
        let thisComponent = this

        axios.delete('/dashboard/employees/' + e.id)
          .then(function (response) {
            // $('#delete-photo-modal').modal('hide')

            thisComponent.dataEntrySuccessMessage = 'Photo deleted successfully.'
            thisComponent.employees.splice(thisComponent.employees.indexOf(e), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.dataEntryErrorMessage = error.response.data.errors[0].detail
              }
            } else {
              thisComponent.dataEntryErrorMessage = 'Unknown error.'
            }
          })
      }
    },
    created () {
      this.queryIslands()
    }
});

</script>



@stop