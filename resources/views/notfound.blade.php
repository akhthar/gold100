@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #B0413E; color: #fff"><h4>404 Not Found</h4></div>

                <div class="panel-body" style="color: #B0413E;">
                    The requested url was not found on this server.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection