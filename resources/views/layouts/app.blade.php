<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>GOLD100 | REGISTRATION</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <meta property="og:title" content="GOLD100 Information Form" />
    <meta property="og:url" content="http://corporatemaldives.com/" />
    <meta property="og:image" content="https://gold100.corporatemaldives.com/images/gold-100.jpg" />

    <!-- Styles -->
    @section('styles')
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/pace/pace.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="css/app.css" rel="stylesheet">
    <style>
        .navbar-inverse {
            background-color: #000;
            border: none;
            color: #ccc !important;
        }

        .navbar-inverse .navbar-brand, .navbar-inverse .navbar-nav>li>a, .navbar-inverse .navbar-text {
            color: #CD9B1D;
        }

        .navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-nav>li>a:hover, .navbar-inverse .navbar-text:hover {
            color: #CDAD00;
        }

        #content {
            margin-top: 80px;
            clear: both;
        }
        #loader {
            /*height: 20px;*/
            text-align: center;
            /*z-index: 1000000;*/
            /*border: 1px solid #000;*/
            /*width: 100%;*/
            /*margin-top: 50px;*/
            /*display: block;*/
              position: fixed;
              z-index: 2000;
              top: 15px;
              right: 50%;
              width: 14px;
              height: 14px;
              border: solid 2px transparent;
              border-top-color: #EEB422;
              border-left-color: #EEB422;
              border-radius: 10px;
              -webkit-animation: pace-spinner 400ms linear infinite;
              -moz-animation: pace-spinner 400ms linear infinite;
              -ms-animation: pace-spinner 400ms linear infinite;
              -o-animation: pace-spinner 400ms linear infinite;
              animation: pace-spinner 400ms linear infinite;
        }

        .navbar-inverse .navbar-toggle {
            border-color: #ccc;
        }
        .navbar-inverse .navbar-toggle .icon-bar {
            background-color: #ccc;
        }

        .panel-default, .panel-default input, .panel-default textarea, .panel-default select, .panel-heading, .panel-body, .panel-footer {
            border-radius: 0;
            -moz-border-radius: 0;
            -webkit-border-radius: 0;
        }

        .form-horizontal .control-label {
            text-align: left;
        }

        [v-cloak] {
          display:none;
        }

        .btn-primary {
            background-color: #bf9b30;
            color: #fff;
            border: none;
        }

        .btn-primary:hover {
            background-color: #A08029;
            color: #fff;
            border: none;
        }

        .italic {
            font-style: italic;
        }

        .note {
            font-size: 12px;
        }

        .highlight {
            color: #F2473F;
        }

        [v-cloak] {
            display: none;
        }

    </style>
@show
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        GOLD100
                    </a>
                </div>
                <div id="app-navbar-collapse" class="collapse navbar-collapse">
                  <ul class="nav navbar-nav navbar-right">
                    <li v-for="navLink in rightNavlinks">
                      <a href="http://corporatemaldives.com"><i class="fa fa-home"></i> Home</a>
                    </li>
                  </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

<!-- ajax loader -->
<div id="loader"></div>

        <div id="content">
        @yield('content')
        </div>
    </div>
@section('scripts')
    <!-- Scripts -->

    <script src="{{ asset('plugins/jQuery/jQuery-2.2.3.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js')}}"></script>
    <!-- <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script> -->
    <!-- <script src="{{ asset('js/app.min.js') }}"></script> -->
    <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('js/vue.js')}}"></script>
    <script src="{{ asset('js/axios.min.js')}}"></script>
    <script src="{{ asset('fine-uploader/fine-uploader/fine-uploader.core.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function(){
            $('#loader').hide();
        });
    </script>

    @show
</body>
</html>
