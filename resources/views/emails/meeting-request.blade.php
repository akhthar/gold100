<html>
	<body>
		<p>Greetings {{ $recipientName }},</p>
		
		
		<p>
		You have a new meeting request at 'CONNECT' Conference Management System of TTM.
		<br/><br/>
		Please login to <a href="http://connect.traveltrademaldives.com">connect.traveltrademaldives.com</a> to proceed.<br/>
		</p>
		
		
		<p>
		Best Regards<br />
		TTM Team
		</p>
	</body>
</html>