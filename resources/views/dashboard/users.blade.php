@extends('dashboard.layout')

@section('styles')
@parent
<style>

</style>
@stop


@section('content')
<div class="row">
	<div class="col-sm-12">
		<span class="component-heading">Users</span>
  <button type="button" class="btn btn-sm btn-primary btn-flat spaced accept pull-right" data-toggle="modal" data-target="#create-user-modal">Create New User</button>


	</div>
</div>

<hr/>


<div class="row table-wrapper">

<div class="col-sm-12">
<table class="table table-striped table-responsive">
      <thead>
        <tr>
        	<th>#</th>
          <th class="col-">Name</th>
          <th class="col-">Login ID</th>
          <th class="col-">Email</th>
          <th class=""></th>
        </tr>
      </thead>
      <tbody>
      
          <tr v-for="u in users">
          <td></td>
          <td><a v-on:click="viewUser(u)" data-toggle="modal" data-target="#view-user-modal" style="cursor: pointer;">@{{ u.name }}</a></td>
          <td>@{{ u.username }}</td>
          <td>@{{ u.email }}</td>
          <td class="pull-right">
          <button v-on:click="confirmPasswordReset(u)" type="button" class="btn btn-xs btn-warning btn-flat spaced accept" data-toggle="modal" data-target="#confirm-password-reset-modal"><i class="fa fa-key"></i> Reset Password</button>
            <button v-on:click="confirmDeleteRequest(u)" type="button" class="btn btn-xs btn-danger btn-flat" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i> Delete</button>
          </td>
        </tr>
       
      </tbody>
      </table>
      </div>
</div>

<!-- create user modal -->

<div class="modal fade" id="create-user-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Create New User</h4>
          </div>

          <div class="modal-body">

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" v-model="dataEntryModel.name"/>
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Login ID</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" v-model="dataEntryModel.username"/>
                </div>
              </div>
              
            <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" v-model="dataEntryModel.email"/>
                </div>
              </div>

              
          </div><br style="clear: both;" />

          <div class="col-sm-12">
          <div v-if="newUserErrorMessage.length > 1" class="alert alert-danger" role="alert" style="margin-top: 5px;">
    @{{ newUserErrorMessage }}
    </div>
          </div>

          <br/>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" v-on:click="createUser()">Save</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
          </div>

        </div>
      </div>
    </div> 
<!-- /create user modal -->

<!-- full view modal -->

<div class="modal fade" id="view-user-modal" tabindex="-1" role="dialog" v-if="user !== null">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">@{{ user.name }}</h4>
          </div>

          <div class="modal-body">

            

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" v-model="user.name">
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Login ID</label>
                <div class="col-sm-8">
                  <div class="form-control disabled">@{{ user.username }}</div>
                </div>
              </div>
              
            <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" v-model="user.email">
                </div>
              </div>

          </div><br style="clear: both;" />

          <br/>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" v-on:click="updateUser(user)">Update</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" v-on:click="resetUser()">Close</button>
          </div>

        </div>
      </div>
    </div> 
<!-- full view modal -->



    <!-- delete confirmation modal -->
    <div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Delete User</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">Are you sure to delete this user?</label>
                
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-flat" v-on:click="deleteUser(deleteModel.obj)">Yes</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
          </div>

        </div>
      </div>
    </div> <!-- /modal-->

    <!-- password reset confirmation modal -->
    <div class="modal fade in" id="confirm-password-reset-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Reset Password</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">Are you sure reset the password for this user?</label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-flat" id="accept-request-button" v-on:click="resetPassword(deleteModel.obj)">Yes</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
          </div>

        </div>
      </div>
    </div> <!-- /modal-->
@endsection


@section('scripts')
@parent

<script>

new Vue ({
    el: '#content',
    data: {
      uiState: {
        basicInfoMsg: '',
        basicInfoState: ''
      },
      dataEntryModel: {},
    	dataEntrySuccessMessage: '',
    	dataEntryErrorMessage: '',
    	pageLoadErrorMessage: '',
      newUserErrorMessage: '',
    	users: [],
    	filters: {
    		page: 1,
        	limit: 20,
       		orderBy: 'created_at',
        	orderDir: 'desc',
        	totalRecords: 0,
        	totalPages: 1,
        	property_name: ''
    	},
      deleteModel: {},
      user: null,
      editUser: null
    },
    methods: {
    	queryUsers (event) {
        if (event) {
          event.preventDefault()
        }

        let thisComponent = this

        axios.get('{{url("admin/get-users")}}', {
          params: thisComponent.filters
        })
        .then(function (response) {

          thisComponent.filters.page = response.data.meta.pager.page
          thisComponent.filters.limit = response.data.meta.pager.limit
          thisComponent.filters.totalRecords = response.data.meta.pager.totalCount
          thisComponent.filters.totalPages = Math.ceil(response.data.meta.pager.totalCount / response.data.meta.pager.limit)

          thisComponent.users = response.data.data
        })
        .catch(function (error) {
          if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          
        })
      },
      confirmPasswordReset (o) {
        this.deleteModel.obj = o
      },
      confirmDeleteRequest (o) {
        this.deleteModel.obj = o
      },
      resetPassword (o) {
        let thisComponent = this
        let data = thisComponent.deleteModel.obj
        axios.get('{{url("admin/reset-password")}}/' + data.id)
          .then(function (response) {
            $('#confirm-password-reset-modal').modal('hide')

            toastr.success('Password reset for ' + data.username + '.', {timeOut: 5000});
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          })
      },
      deleteUser (o) {
        let thisComponent = this
        let data = thisComponent.deleteModel.obj
        axios.delete('{{url("admin/users")}}/' + data.id)
          .then(function (response) {
            $('#confirm-delete-modal').modal('hide')

           toastr.success('User ' + data.username + ' deleted.', {timeOut: 5000});
            thisComponent.users.splice(thisComponent.users.indexOf(data), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          })
      },
      viewUser (o) {
        this.user = o
      },
      resetUser() {
        this.user = null
      },
      createUser () {
      	let data = {
          name: this.dataEntryModel.name,
          username: this.dataEntryModel.username,
          email: this.dataEntryModel.email,
        }

        let thisComponent = this

        axios.post('{{url("admin/users")}}', data)
          .then(function (response) {
            $('#create-user-modal').modal('hide')

            toastr.success('User ' + data.username + ' created.', {timeOut: 10000});
            thisComponent.users.unshift(response.data)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          })
      },
  
      updateUser (user) {
      	let data = user

        let thisComponent = this

        axios.put('{{url("admin/users")}}/' + user.id, data)
          .then(function (response) {
            $('#view-user-modal').modal('hide')

            toastr.success('User ' + data.username + ' updated.', {timeOut: 10000});
  	        
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          })
      },
      resetPassword (n) {
        let thisComponent = this

        axios.get('{{url("admin/reset-password")}}/' + n.id)
          .then(function (response) {
            $('#confirm-password-reset-modal').modal('hide')

            toastr.success('Password reset for user: ' + n.username + '.', {timeOut: 5000});
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 5000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 5000});
            }
          })
      }

    }, // end methods
    created () {
      this.queryUsers()
    }
});

</script>

@stop

@section('styles')
@parent



@stop