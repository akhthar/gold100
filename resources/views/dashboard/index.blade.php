@extends('dashboard.layout')

@section('styles')
@parent

@stop

@section('scripts')
@parent

@stop

@section('content')
	<div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $newRequestsCount }}</h3>

              <h4>New Registration Requests</h4>
            </div>
            <div class="icon">
              <i class="ion ion-information-circled"></i>
            </div>
            <a href="{{url('admin/requests')}}" class="small-box-footer">View Requests <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $registeredClientsCount }}</h3>

              <h4>Accepted Companies</h4>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark-circled"></i>
            </div>
            <a href="{{url('admin/accepted-companies')}}" class="small-box-footer">View Companies <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $usersCount }}</h3>

              <h4>Users</h4>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="{{url('admin/users')}}" class="small-box-footer">View Users <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
@endsection

@section('scripts')
@parent

<script>

new Vue ({
    el: '#content'
  });

</script>
@stop