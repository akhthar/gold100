<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GOLD100 REGISTRATION | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  @section('styles')

  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/pace/pace.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <!-- </head> -->

    

  <!-- Bootstrap 3.3.6 -->
  <!-- <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}"> -->
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <!-- <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}"> -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css') }}">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/iCheck/flat/blue.css') }}"> -->
  <!-- Date Picker -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}"> -->
  <!-- Daterange picker -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}"> -->
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style>
  .user-header h2 {
    color: #fff;
    font-weight: bolder;
  }
    hr {
      border-color: #999999;
    }
    .logout {
      background-color: #3c8dbc !important;
      border-color: #367fa9 !important;
    }
    .logout:hover {
      background-color: #337aa7 !important;
      border-color: #337ab7 !important;
    }
    .table-wrapper {
    overflow-x: auto;
  }

  .form-horizontal {
    padding: 10px 20px;
  }
  .component-heading {
    font-weight: bold;
    font-size: 24px;
  }
  table {
    background-color: #fdfdfd;
  }
  @media (max-width: 768px) {
    .spaced {
      margin-bottom: 5px;
    }
  }

  .green:hover {
    background-color: #4AC948;
    color: #CCFFCC;
  }
  .orange:hover {
    background-color: #FF7722;
    color: #FFEFDB;
  }
  .red:hover {
    background-color: #FC1501;
    color: #FFAAAA;
  }


  .form-group {
    margin-bottom: 10px !important;
    clear: both;
  }
  .form-control, .control-label {
    margin-bottom: 10px !important;
    clear: both;
  }
  .disabled {
    color: #ccc;
    /*font-style: italic;*/
  }

  [v-cloak] {
    display: none;
  }

  </style>
  @show
</head>
<body class="hold-transition skin-yellow sidebar-mini fixed ">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('admin')}}" class="logo">
      <span class="logo-mini"><small><b>GOLD100</b></small><br/>Admin</span>
      <span class="logo-lg"><b>GOLD100</b> Admin</span>
    </a>
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- notifications -->
          
          <!-- / notifications -->

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ Auth()->user()->name }}</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
                <h2>{{ Auth()->user()->name }}</h2>
                <p>

                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#change-password-modal">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-primary btn-flat logout">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li @if($selected_menu == 'dashboard') class="active" @endif>
          <a href="{{url('admin/dashboard')}}">
          <i class="fa fa-dashboard"></i> 
          <span>Dashboard</span>
          </a>
        </li>

        <li class="treeview @if($parent_menu == 'properties') active @endif">
          <a href="#">
            <i class="fa fa-building-o"></i>
            <span>Companies</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li @if($selected_menu == 'new-requests') class="active" @endif><a href="{{url('/admin/requests')}}"><i class="fa fa-bell-o"></i> New Requests</a></li>

             <li @if($selected_menu == 'registered') class="active" @endif><a href="{{url('/admin/accepted-companies')}}"><i class="fa fa-certificate"></i> Accepted Companies</a></li>

             <li @if($selected_menu == 'rejected') class="active" @endif><a href="{{url('/admin/rejected')}}"><i class="fa fa-ban"></i> Rejected Companies</a></li>
            
          </ul>
        </li>

       

        <li @if($selected_menu == 'users') class="active" @endif><a href="{{url('/admin/users')}}"><i class="glyphicon glyphicon-user"></i> <span>Users</span></a></li>
        <!-- <li @if($selected_menu == 'settings') class="active" @endif><a href="/admin/settings"><i class="fa fa-gears"></i> <span>Sytem Settings</span></a></li> -->
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" id="content" v-cloak>
       @yield('content')

       

    </section>

<div id="password">
  <!-- password change modal -->

<div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Change Password</h4>
          </div>

          <div class="modal-body">

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">New Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" v-model="passwordModel.new_password"/>
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-4 control-label">Confirm Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" v-model="passwordModel.repeat_new_password" :keyup="comparePasswords()"/>
                </div>
              </div>
              
           

          </div><br style="clear: both;" />
          <!-- <div class="col-sm-12">
          <div v-if="uiPasswordChangeState.basicInfoMsg.length > 1" class="alert alert-danger" role="alert" style="margin-top: 5px;">
    @{{ uiPasswordChangeState.basicInfoMsg }}
    </div>
    </div> -->
          <br/>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" v-bind:disabled="((passwordModel.new_password !== passwordModel.repeat_new_password) || (passwordModel.new_password == '' || passwordModel.repeat_new_password == ''))" v-on:click="changePassword()">Change Password</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
          </div>

        </div>
      </div>
    </div> 
<!-- /password change modal -->

<!-- <div class="modal fade in" id="password-change-complete-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Password Change</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">@{{ uiPasswordChangeState.basicInfoMsg }}</label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>
</div> -->




     <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <small><b>Version</b> 1.0.3</small>
    </div>
    <small><strong>Copyright &copy; 2017 HYDRA-CMS.</strong> HYDRA TECH All rights
    reserved.</small>
  </footer> -->
  
</div>
<!-- ./wrapper -->

@section('scripts')

<!-- built files will be auto injected. Static files below -->
    <script src="{{ asset('plugins/jQuery/jQuery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>

<!-- jQuery 2.2.3 -->
<!-- <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.0/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>

<!-- Bootstrap 3.3.6 -->
<!-- <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script> -->
<!-- Sparkline -->
<!-- <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script> -->
<!-- daterangepicker -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
<!-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script> -->
<!-- datepicker -->
<!-- <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<!-- <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> -->
<!-- Slimscroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- FastClick -->
<!-- <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script> -->
<!-- AdminLTE App -->
<!-- <script src="{{ asset('dist/js/app.min.js') }}"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="{{ asset('dist/js/demo.js') }}"></script> -->
<script>
toastr.options = {
  "positionClass": "toast-bottom-full-width"//"toast-top-center"
}
</script>

<script>
  new Vue ({
    el: '#password',
    data: {
      uiPasswordChangeState: {
        basicInfoMsg: '',
        basicInfoState: ''
      },
      passwordModel: {
        new_password: '',
        repeat_new_password: ''
      },
      buttonDisabled: true
    },
    methods: {
      changePassword () {
        let thisComponent = this
        let data = thisComponent.passwordModel
        axios.post('{{url("/admin/users/change-password")}}', data)
          .then(function (response) {
            $('#change-password-modal').modal('hide')
          //  $('#password-change-complete-modal').modal('show')

            // thisComponent.uiPasswordChangeState.basicInfoMsg = 'Your password has been changed succussfully.'
            toastr.success('Your password has been changed succussfully.', {timeOut: 10000});
            // thisComponent.uiState.basicInfoMsg = 'success'
            // thisComponent.uiState.basicInfoMsg = 'Request by ' + data.property_name + ' accepted.'
           // thisComponent.dataEntrySuccessMessage = 'User ' + data.username + ' deleted.'
            // thisComponent.properties.unshift(response.data)
            // thisComponent.users.splice(thisComponent.users.indexOf(data), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                // thisComponent.uiState.basicInfoMsg = 'error'
                toastr.error(error.response.data.errors[0].detail, {timeOut: 10000});
              }
            } else {
              // thisComponent.uiState.basicInfoMsg = 'error'
             // thisComponent.uiPasswordChangeState.basicInfoMsg = 'Unknown error.'
              toastr.error('Unknown error.', {timeOut: 10000});
            }
          })
      },
      comparePasswords () {
        let thisComponent = this
       // thisComponent.passwordModel.new_password = thisComponent.passwordModel.repeat_new_password
        if (thisComponent.passwordModel.new_password !== thisComponent.passwordModel.repeat_new_password) {
          // thisComponent.uiPasswordChangeState.basicInfoMsg = 'Both passwords do not match'
          buttonDisabled = true
        } else {
          // thisComponent.uiPasswordChangeState.basicInfoMsg = ''
          buttonDisabled = false
        }
      }
    }
  });
</script>

@show
</body>
</html>
