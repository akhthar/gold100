@extends('dashboard.layout')

@section('styles')
@parent
<style>

</style>
@stop


@section('content')
<div class="row">
	<div class="col-sm-12">
		<span class="component-heading">New Requests for Registration</span>
	</div>
	</div>

<hr/>


<div class="row table-wrapper">

<div class="col-sm-12">
<table class="table table-striped table-responsive">
      <thead>
        <tr>
          <th>#</th>
          <th>Logo</th>
          <th>Registration</th>
          <th class="col-2">Company Name</th>
          <th class="col-1">Managing Director</th>
          <th class="col-2">Email</th>
          <th class="col-2">Website</th>
          <th class="col-1">Contact Number</th>
          <th class="col-1">Contact Name</th>
          <th class="col-1">Contact Person Number</th>
          <th class="col-1">Contact Email</th>
          <th class="col-">Requested On</th>
          <th class=""></th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="p in properties">
          <td></td>
          <td><a :href="getLogoPath(p)" target="_blank"><img :src="getLogoPath(p)" class="img-responsives" style="width: 100px;" /></a></td>
          <td><a :href="getRegistrationPath(p)" target="_blank"><img :src="getRegistrationPath(p)" class="img-responsives" style="width: 100px;" /></a></td>
          <td><a v-on:click="viewProperty(p)" data-toggle="modal" data-target="#view-company-modal" style="cursor: pointer;">@{{ p.company_name }}</a></td>
          <td>@{{ p.managing_director }}</td>
          <td>@{{ p.email }}</td>
          <td>@{{ p.website }}</td>
          <td>@{{ p.contact_number }}</td>
          <td>@{{ p.contact_person }}</td>
          <td>@{{ p.contact_person_number }}</td>
          <td>@{{ p.contact_email }}</td>
          <td>@{{ getDateTime(p.created_at) }}</td>
          <td class="pull-right">
          <button v-on:click="confirmAcceptRequest(p)" type="button" class="btn btn-xs btn-success btn-flat spaced accept" data-toggle="modal" data-target="#confirm-registration-modal"><i class="fa fa-check"></i> Accept</button>

          <button v-on:click="confirmAcceptRequest(p)" type="button" class="btn btn-xs btn-warning btn-flat spaced accept" data-toggle="modal" data-target="#confirm-reject-modal"><i class="fa fa-ban"></i> Reject</button>

            <button v-on:click="confirmDeleteRequest(p)" type="button" class="btn btn-xs btn-danger btn-flat" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i> Delete</button>
          </td>
        </tr>
       
      </tbody>
      </table>
      </div>
</div>

<!-- full view modal -->

<div class="modal fade" id="view-company-modal" tabindex="-1" role="dialog" v-if="company !== null">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">@{{ company.company_name }}</h4>
          </div>

          <div class="modal-body">

            <div class="form-group">
                
                <div class="col-sm-12">
                LOGO<br/>
                  <img :src="getLogoPath(company)" class="img-responsive" />
                </div>
              </div><br style="clear: both;" /><br/>

              <div class="form-group">
                
                <div class="col-sm-12">
                REGISTRATION<br/>
                  <img :src="getRegistrationPath(company)" class="img-responsive" />
                </div>
              </div><br style="clear: both;" /><br/>

              <div class="form-group">
                <label for="first_name" class="col-sm-12 control-label">Company Name</label>
                <div class="col-sm-12">
                  <div class="form-control">@{{ company.company_name }}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-12 control-label">Company Registration Number</label>
                <div class="col-sm-12">
                  <div class="form-control">@{{ company.registration_number }}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-12 control-label">Managing Director</label>
                <div class="col-sm-12">
                  <div class="form-control">@{{ company.managing_director }}</div>
                </div>
              </div>
              
              <div class="form-group">
                <label for="year_started" class="col-sm-12 control-label">Registration Date </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{ getDate(company.registration_date) }}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="employment" class="col-sm-12 control-label">Number of Employees </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.number_of_employees}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="revenue" class="col-sm-12 control-label">Annual Gross Revenue </label>
                <div class="col-sm-12">
                  <label class="control-label">2022</label>
                  <div class="form-control">@{{ company.revenue_currency_2017 }} @{{company.annual_gross_revenue_2017}}</div>
                </div>

                <div class="col-sm-12">
                  <label class="control-label">2021</label>
                  <div class="form-control">@{{ company.revenue_currency_2016 }} @{{company.annual_gross_revenue_2016}}</div>
                </div>

                <div class="col-sm-12">
                  <label class="control-label">2020</label>
                  <div class="form-control">@{{ company.revenue_currency_2015 }} @{{company.annual_gross_revenue_2015}}</div>
                </div>
              </div>


              <div class="form-group">
                <label for="industry" class="col-sm-12 control-label">Industry </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.industry}}</div>
                </div>
              </div>


              <div class="form-group">
                <label for="subsidiaries" class="col-sm-12 control-label">Subsidiaries </label>
                <div class="col-sm-12" v-for="(sub, index) in company.subsidiaries">
                  <div class="form-control">@{{ index+1 }} - @{{sub.name}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="introduction" class="col-sm-12 control-label">Company Introduction (100 - 120 words)</label>
                <div class="col-sm-12">
                  <div class="form-control" style="height: auto;">@{{company.introduction}}</div><br style="clear: both;"/>
                </div>
              </div>

              <div class="form-group">
                <label for="products_and_services" class="col-sm-12 control-label">Products & Services (60 - 80 words)</label>
                <div class="col-sm-12">
                  <div class="form-control" style="height: auto;">@{{company.products_and_services}}</div><br style="clear: both;"/>
                </div>
              </div>

              <div class="form-group">
                <label for="business_success_and_innovativeness" class="col-sm-12 control-label">History of Business Success, Innovativeness and CSR Innitiatives </label>
                <div class="col-sm-12">
                  <div class="form-control" style="height: auto;">@{{company.business_success_and_innovativeness}}</div><br style="clear: both;"/>
                </div>
              </div>

              <div class="form-group">
                <label for="address" class="col-sm-12 control-label">Address </label>
                <div class="col-sm-12">
                  <div class="form-control" style="height: auto;">@{{company.address}}</div><br style="clear: both;"/>
                </div>
              </div>


              <div class="form-group">
                <label for="contact_number" class="col-sm-12 control-label">Contact Number </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.contact_number}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-sm-12 control-label">Email Address </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.email}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="website" class="col-sm-12 control-label">Website </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.website}}</div>
                </div>
              </div>


              <div class="form-group">
                <label for="contact_number" class="col-sm-12 control-label">Contact Name </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.contact_person}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="contact_number" class="col-sm-12 control-label">Contact Person Number </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.contact_person_number}}</div>
                </div>
              </div>

              <div class="form-group">
                <label for="contact_number" class="col-sm-12 control-label">Contact Email </label>
                <div class="col-sm-12">
                  <div class="form-control">@{{company.contact_email}}</div>
                </div>
              </div>


              <div class="form-group">
                <label for="ip_address" class="col-sm-12 control-label">Requested IP Address</label>
                <div class="col-sm-12">
                  <div class="form-control">@{{ company.client_ip_address }}</div>
                </div>
              </div>


          </div><br style="clear: both;" />

          <br/>

          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div> 
<!-- full view modal -->



    <!-- delete confirmation modal -->
    <div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Delete Request</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">Are you sure to delete this request?</label>
                
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-flat" v-on:click="deleteRequest(deleteModel.obj)">Yes</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
          </div>

        </div>
      </div>
    </div> <!-- /modal-->

    <!-- accept confirmation modal -->
    <div class="modal fade in" id="confirm-registration-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Accept Registration</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">Are you sure to accept this request for registration?</label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" id="accept-request-button" v-on:click="acceptRequest(deleteModel.obj)">Yes</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
          </div>

        </div>
      </div>
    </div> <!-- /modal-->


    <!-- reject confirmation modal -->
    <div class="modal fade in modal-warning" id="confirm-reject-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteModalLabel">Reject Registration</h4>

          </div>

          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label  class="col-sm-12">Are you sure to REJECT this request for registration?</label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-flat" id="accept-request-button" v-on:click="rejectRequest(deleteModel.obj)">Yes</button>
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
          </div>

        </div>
      </div>
    </div> <!-- /modal-->
@endsection


@section('scripts')
@parent

<script>

new Vue ({
    el: '#content',
    data: {
      uiState: {
        basicInfoMsg: '',
        basicInfoState: ''
      },
    	dataEntrySuccessMessage: '',
    	dataEntryErrorMessage: '',
    	pageLoadErrorMessage: '',
    	properties: [],
    	filters: {
    		page: 1,
        	limit: 20,
       		orderBy: 'created_at',
        	orderDir: 'desc',
        	totalRecords: 0,
        	totalPages: 1,
        	property_name: ''
    	},
      deleteModel: {},
      company: null
    },
    methods: {
    	queryNewRequests (event) {
        if (event) {
          event.preventDefault()
        }

        let thisComponent = this

        axios.get('{{url("admin/new-requests")}}', {
          params: thisComponent.filters
        })
        .then(function (response) {

         
          thisComponent.properties = response.data
        })
        .catch(function (error) {
          if (error.response) {
            if (error.response.data && error.response.data.errors) {
              toastr.error(error.response.data.errors[0].detail, {timeOut: 10000});
            }
          } else {
            toastr.error('Unknown error.', {timeOut: 10000});
          }
          console.log(error)
        })
      },
      confirmAcceptRequest (o) {
        this.deleteModel.obj = o
      },
      confirmDeleteRequest (o) {
        this.deleteModel.obj = o
      },
      acceptRequest (o) {
        let thisComponent = this
        let data = thisComponent.deleteModel.obj
        axios.get('{{url("admin/accept-request")}}/' + data.id)
          .then(function (response) {
            $('#confirm-registration-modal').modal('hide')

            toastr.success('Request by ' + data.company_name + ' accepted.', {timeOut: 10000});
            thisComponent.properties.splice(thisComponent.properties.indexOf(data), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 10000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 10000});
            }
          })
      },
      deleteRequest (o) {
        let thisComponent = this
        let data = thisComponent.deleteModel.obj
        axios.delete('{{url("admin/request")}}/' + data.id)
          .then(function (response) {
            $('#confirm-delete-modal').modal('hide')

            toastr.success('Request by ' + data.company_name + ' deleted.', {timeOut: 10000});
            thisComponent.properties.splice(thisComponent.properties.indexOf(data), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 10000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 10000});
            }
          })
      },
      rejectRequest (o) {
        let thisComponent = this
        let data = thisComponent.deleteModel.obj
        axios.get('{{url("admin/reject-request")}}/' + data.id)
          .then(function (response) {
            $('#confirm-reject-modal').modal('hide')

            toastr.success('Request by ' + data.company_name + ' rejected.', {timeOut: 10000});
            thisComponent.properties.splice(thisComponent.properties.indexOf(data), 1)
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                toastr.error(error.response.data.errors[0].detail, {timeOut: 10000});
              }
            } else {
              toastr.error('Unknown error.', {timeOut: 10000});
            }
          })
      },
      viewProperty (o) {
        this.company = o
      },
      
      confirmDeleteItem (n) {
        this.deleteModel.obj = n
      },
      
      getLogoPath (p) {
        return '../company_logos/' + p.company_logo
      },

      getRegistrationPath (p) {
        return '../company_registrations/' + p.company_registration
      },

      getDateTime (dateStr) {
        return moment(dateStr, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY HH:mm:ss')
      },

      getYear (dateStr) {
        return moment(dateStr, 'YYYY-MM-DD HH:mm:ss').format('YYYY')
      },

      getDate (dateStr) {
        return moment(dateStr, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY')
      }

    }, // end methods
    created () {
      this.queryNewRequests()
    }
});

</script>

@stop

@section('styles')
@parent

<style>
 <style>
  
  .control-label {
    text-align: left !important;
  }
  .form-group {
    margin-bottom: 10px !important;
    clear: both;
  }
  .form-control, .control-label {
    margin-bottom: 10px !important;
    clear: both;
  }
  .input-group-addon {
    height: 10px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    
  }
</style>
</style>

@stop../