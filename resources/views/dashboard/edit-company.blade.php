@extends('dashboard.layout')

@section('styles')
@parent
<style>
.form-horizontal .control-label {
            text-align: left;
        }
</style>
@stop




@section('content')
<div class="container" v-cloak>
  @if(1==0)
    <!-- registration closed -->
    <div class="row">
      <div class="col-md-10">
        <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;"><h3>REGISTRATION CLOSED</h3></div>
               
                <div class="panel-body" style="padding: 40px 40px 40px 40px;">
                Please email <a href="mailto:editor@corporatemaldives.com">editor@corporatemaldives.com</a> or contact (960) 301-1720 for more information.
                </div>

                
                 </div>
      </div>
    </div>
  @else
    <!-- if not registration closed-->
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center; background-color: #bf9b30; color: #fff;"><h3>INFORMATION FORM</h3></div>

                <div class="panel-body" style="padding: 40px 40px 10px 40px;">
                <span class="pull-right note highlight">Fields marked with * are required.</span>
                    <form class="form-horizontal">

              <!-- company name -->
              <div class="form-group">
                <label for="company_name" class="col-sm-12 control-label">COMPANY NAME (as it appears on the Company Registration Certificate) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="company_name" placeholder="COMPANY NAME" v-model="dataModel.company_name"/>
                </div>
              </div>
              <!-- ./company name -->

              <!-- company registration -->
              <div class="form-group">
                <label for="registration_number" class="col-sm-12 control-label">COMPANY REGISTRATION NO. (as it appears on the Company Registration Certificate) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="registration_number" placeholder="COMPANY REGISTRATION NO." v-model="dataModel.registration_number"/>
                </div>
              </div>
              <!-- ./company registration -->

              <!-- managing director -->
              <div class="form-group">
                <label for="managing_director" class="col-sm-12 control-label">Managing Director <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="managing_director" placeholder="Managing Director" v-model="dataModel.managing_director"/>
                </div>
              </div>
              <!-- ./managing director -->

              <!-- registration date -->
              <div class="form-group">
                <label for="registration_date" class="col-sm-12 control-label">Registration Date <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="registration_date" placeholder="Registration Date" v-model="dataModel.registration_date"/>
                </div>
              </div>
              <!-- ./registration date -->

              <!-- number of employees -->
              <div class="form-group">
                <label for="number_of_employees" class="col-sm-12 control-label">Number of employees (at the end of the accounting period ended in 2016) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="number_of_employees" placeholder="Number of employees" v-model="dataModel.number_of_employees"/>
                </div>
              </div>
              <!-- ./number of employees -->

              <!-- annual gross revenue -->
              <div class="form-group">
                <label for="revenue" class="col-sm-12 control-label">Annual Gross Revenue <span class="highlight">*</span></label><br/>

                <div class="col-sm-12" style="text-align: justify;">
                  <input type="checkbox" v-model="earnsMoreThanTenMillion" /> 
                  <span style="font-size: 13px;">I hereby declare that the above mentioned company or group of companies earned a revenue of MVR 10 million or more during the financial year of 2016.
                  </span>
                </div>

                <br style="clear: both;" /><br/>

                <div class="col-sm-12">Please specify details:</div>
                
                <!-- 2016 -->
                <div class="col-sm-12" style="margin-bottom: 5px;">
                  <div class="col-sm-1 no-padding" style="padding-top: 6px !important;">
                    2016
                  </div>
                  <div class="col-sm-2 no-padding">
                    <select id="revenue_currency_2016" class="form-control" v-model="dataModel.revenue_currency_2016">
                      <option value="MVR" >MVR</option>
                      <option value="USD">USD</option>
                    </select>
                  </div>
                  <div class="col-sm-9 no-padding">
                      <input type="text" class="form-control" id="annual_gross_revenue_2016" placeholder="Annual Gross Revenue 2016" v-model="dataModel.annual_gross_revenue_2016"/>
                  </div>
                </div>
                <!-- ./2016 -->

                <!-- 2015 -->
                <div class="col-sm-12" style="margin-bottom: 5px;">
                  <div class="col-sm-1 no-padding" style="padding-top: 6px !important;">
                    2015
                  </div>
                  <div class="col-sm-2 no-padding">
                    <select id="revenue_currency_2015" class="form-control" v-model="dataModel.revenue_currency_2015">
                      <option value="MVR" >MVR</option>
                      <option value="USD">USD</option>
                    </select>
                  </div>
                  <div class="col-sm-9 no-padding">
                      <input type="text" class="form-control" id="annual_gross_revenue_2015" placeholder="Annual Gross Revenue 2015" v-model="dataModel.annual_gross_revenue_2015"/>
                  </div>
                </div>
                <!-- ./2015 -->

                <!-- 2014 -->
                <div class="col-sm-12" style="margin-bottom: 5px;">
                  <div class="col-sm-1 no-padding" style="padding-top: 6px !important;">
                    2014
                  </div>
                  <div class="col-sm-2 no-padding">
                    <select id="revenue_currency_2014" class="form-control" v-model="dataModel.revenue_currency_2014">
                      <option value="MVR" >MVR</option>
                      <option value="USD">USD</option>
                    </select>
                  </div>
                  <div class="col-sm-9 no-padding">
                      <input type="text" class="form-control" id="annual_gross_revenue_2014" placeholder="Annual Gross Revenue 2014" v-model="dataModel.annual_gross_revenue_2014"/>
                  </div>
                </div>
                <!-- ./2014 -->

              </div>
              <!-- ./annual gross revenue -->

              <!-- industry -->
              <div class="form-group">
                <label for="industry" class="col-sm-12 control-label">Industry <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <select class="form-control" id="industry" placeholder="Industry" v-model="dataModel.industry">
                    <option value=""></option>
                    <option value="Arts, entertainment and recreation">Arts, entertainment and recreation</option>
                    <option value="Aviation"> Aviation</option>
                    <option value="Construction"> Construction</option>
                    <option value="Diving and water sports activities">Diving and water sports activities</option>
                    <option value="Education and healthcare">Education and healthcare</option>
                    <option value="Financial and insurance">Financial and insurance</option>
                    <option value="Food service activities">Food service activities</option>
                    <option value="Information and communication">Information and communication</option>
                    <option value="Professional services">Professional services</option>
                    <option value="Real estate">Real estate</option>
                    <option value="Spa">Spa</option>
                    <option value="Technical services">Technical services</option>
                    <option value="Telecommunications">Telecommunications</option>
                    <option value="Tourist guesthouses">Tourist guesthouses</option>
                    <option value="Tourist hotels">Tourist hotels</option>
                    <option value="Tourist resorts">Tourist resorts</option>
                    <option value="Travel agent">Travel agent</option>
                    <option value="Wholesale and retail trade">Wholesale and retail trade</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
              <!-- ./industry -->

              <!-- subsidiaries -->
              <div class="form-group">
                <label for="subsidiaries" class="col-sm-12 control-label">Subsidiaries</label>
                <div class="col-sm-12" v-for="(sub, index) in subsidiaries" style="margin-bottom: 5px;">
                 
                  <div class="col-xs-11 col-sm-11 no-padding">
                    <input type="text" class="form-control" id="subsidiaries" placeholder="Subsidiary" v-model="sub.subsidiary"/>
                  </div>
                  <div class="col-xs-1 col-sm-1 subsidiary-remove-button no-padding">
                    <button v-on:click="removeSubsidiaryRow(sub)" class="btn btn-flat btn-danger pull-right"><i class="fa fa-close"></i></button>
                  </div>
                </div>
                <div class="col-sm-12 subsidiary-add-button" >
                  
                    <button class="btn btn-flat btn-success" v-on:click="addSubsidiaryRow()"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <!-- ./subsidiaries -->


              <!-- company introduction -->
              <div class="form-group">
                <label for="introduction" class="col-sm-12 control-label">Company Introduction (100 - 120 words) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <textarea class="form-control textarea" id="introduction" placeholder="Company Introduction" v-model="dataModel.introduction" v-on:keyup="wordCount(dataModel.introduction, 'introduction')" rows="8"></textarea>
                  <!-- <span class="italic note">Words: @{{wordCounterIntroduction}}</span> -->
                  <!-- <span :class="classes.introductionLimitWarningClass">@{{ introductionLimitWarning }}</span> -->
                </div>
              </div>
              <!-- ./company introduction -->


              <!-- products & services -->
              <div class="form-group">
                <label for="products_and_services" class="col-sm-12 control-label">Products & Services (60 - 80 words) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <textarea class="form-control textarea" id="products_and_services" placeholder="Products & Services" v-model="dataModel.products_and_services" v-on:keyup="wordCount(dataModel.products_and_services, 'products_and_services')" rows="8"></textarea>
                  <!-- <span class="italic note">Words: @{{wordCounterProductsAndServices}}</span> -->
                  <!-- <span :class="classes.productsLimitWarningClass">@{{ productsLimitWarning }}</span> -->
                </div>
              </div>
              <!-- ./products & services -->

              <!-- history of business success -->
              <div class="form-group">
                <label for="business_success_and_innovativeness" class="col-sm-12 control-label">History of Business Success, Innovativeness and CSR Innitiatives (50 - 200 words) <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <textarea class="form-control textarea" id="business_success_and_innovativeness" placeholder="History of Business Success, Innovativeness and CSR Innitiatives" v-model="dataModel.business_success_and_innovativeness" v-on:keyup="wordCount(dataModel.business_success_and_innovativeness, 'csr')" rows="8"></textarea>
                  <!-- <span class="italic note">Words: @{{wordCounterCSR}}</span> -->
                  <!-- <span :class="classes.csrLimitWarningClass">@{{ csrLimitWarning }}</span> -->
                </div>
              </div>
              <!-- ./history of business success -->

              <!-- address -->
              <div class="form-group">
                <label for="address" class="col-sm-12 control-label">Address <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <textarea class="form-control textarea" id="address" placeholder="Address" v-model="dataModel.address"></textarea>
                </div>
              </div>
              <!-- ./address -->

              <!-- Contact Number-->
              <div class="form-group">
                <label for="contact_number" class="col-sm-12 control-label">Contact Number <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="contact_number" placeholder="Contact Number" v-model="dataModel.contact_number"/>
                </div>
              </div>
              <!-- ./Contact Number-->

              <!-- Business Email Address -->
              <div class="form-group">
                <label for="email" class="col-sm-12 control-label">Business Email Address <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="email" placeholder="Business Email Address" v-model="dataModel.email"/>
                </div>
              </div>
              <!-- ./Business Email Address -->

              <!-- website -->
              <div class="form-group">
                <label for="website" class="col-sm-12 control-label">Website <span class="highlight">*</span></label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="website" placeholder="Website" v-model="dataModel.website">
                </div>
              </div>
              <!-- ./website -->

              <!-- Contact details of the person to contact with regard to this application -->
              <div class="form-group">
                <label for="contact_person" class="col-sm-12 control-label">Contact details of the person to contact with regard to this application <span class="highlight">*</span></label>

                <div class="col-sm-12">

                  <div class="col-sm-4" style="padding-left: 0;">
                    <input type="text" class="form-control" id="contact_person" placeholder="Contact Name" v-model="dataModel.contact_person"/>
                    
                  </div>

                  <div class="col-sm-4" style="padding-left: 0;">
                    <input type="text" class="form-control" id="subsidiaries" placeholder="Contact Number" v-model="dataModel.contact_person_number"/>
                  </div>

                  <div class="col-sm-4" style="padding: 0;">
                    <input type="text" class="form-control" id="contact_email" placeholder="Contact Email" v-model="dataModel.contact_email"/>
                  </div>

                </div>
                
              </div>
              <!-- ./Contact details of the person to contact with regard to this application -->

              


              <hr/>

              
              <!-- robcheck -->
    <p class="robotic" id="pot">
      <label>If you're human leave this blank:</label>
      <input name="rob" type="text" id="rob" class="rob" v-model="dataModel.rob"/>
    </p>
    <!-- ./robcheck -->
              <!-- 
              <div class="form-group">
                <label for="photo" class="col-sm-12 control-label"></label>
                <div class="col-sm-12" style="text-align: justify;">
                  <input type="checkbox" v-model="agreedToTerms"/> 
                  <span style="font-size: 13px;">By checking this box, I agree that all the information provided here is true to the best of my knowledge. I also agree
                  that I am authorized to submit the above information on behalf of the company or group of companies. I hereby
                  authorize Corporate Maldives- A Maldives Getaways subsidiary to include the above-provided information to be cross
                  checked with the Gold 100 selection criteria. I also hereby grant Corporate Maldives access to verify the above
                  information with the assistance of relevant authorities and governing bodies for the purposes including but not limited
                  to Gold 100 selection purposes.
                  </span>
                </div>
              </div> -->
 
            <div v-if="uiState.basicInfoState === 'success'" class="alert alert-success text-center" role="alert">
              @{{ uiState.basicInfoMsg }}
            </div>
            <div v-if="uiState.basicInfoState === 'error'" class="alert alert-danger text-center" role="alert">
              @{{ uiState.basicInfoMsg }}
            </div>


            </form>
                </div>

                <div class="panel-footer" style="text-align: right; padding: 20px 40px;">
                    <button type="button" class="btn btn-primary btn-flat" v-on:click="registerCompany()">Update</button>
                    
                </div>

            </div>
        </div>
    </div>
    @endif
</div>

<div class="modal fade in" id="modal-confirm-delete-logo" style="display: block; padding-right: 17px; display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Logo Deletion!</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure to delete this logo?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-flat" v-on:click="deleteLogo()">Yes</button>
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade in" id="modal-confirm-delete-registration" style="display: block; padding-right: 17px; display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Registration Copy Deletion!</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure to delete this copy of company registration?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-flat" v-on:click="deleteRegistration()">Yes</button>
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

@endsection

@section('scripts')
@parent

<!-- <script src="{{ asset('plugins/jQuery/jQuery-2.2.3.min.js') }}"></script> -->
<!-- <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script> -->
    <!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
    <!-- <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script> -->
    <!-- <script src="{{ asset('js/app.min.js') }}"></script> -->
    <!-- <script src="{{ asset('plugins/pace/pace.min.js') }}"></script> -->

<!-- <script src="{{ asset('js/vue.js') }}"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.0/vue.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script> -->
    <!-- <script src="{{ asset('fine-uploader/fine-uploader/fine-uploader.core.js') }}"></script> -->
    <!-- <script src="{{ asset('js/moment.min.js') }}"></script> -->
    <script src="{{ asset('bootstrap/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script> -->

<!-- <script src="https://cdn.ckeditor.com/ckeditor5/0.11.0/balloon/ckeditor.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/ckeditor5-build-balloon/0.11.0/build/ckeditor.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/0.11.0/balloon/ckeditor.js"></script> -->

<!-- <script>
      BalloonEditor
        .create( document.querySelector( '#introduction' ) )
        .then( editor => {
          console.log( editor );
        } )
        .catch( error => {
          console.error( error );
        } );
    </script> -->

<script>
// $(function () {
//   $('.textarea').wysihtml5({
//       toolbar: {
//         "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
//         "emphasis": true, //Italics, bold, etc. Default true
//         "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
//         "html": false, //Button which allows you to edit the generated HTML. Default false
//         "link": false, //Button to insert a link. Default true
//         "image": false, //Button to insert an image. Default true,
//         "color": false, //Button to change color of font  
//         "blockquote": false, //Blockquote  
//         "size": 'xs' //default: none, other options are xs, sm, lg
//       }
//   });
// });

new Vue ({
    el: '#content',
    data: {
      companyId: {{$companyId}},
      agreedToTerms: '',
      earnsMoreThanTenMillion: 'true',
        dataModel: {
          introduction: '',
          products_and_services: '',
          revenue_currency_2016: 'MVR',
          revenue_currency_2015: 'MVR',
          revenue_currency_2014: 'MVR'
        },
      subsidiaries: [
        
      ],
      companyLogo: '',
      companyRegistration: '',
      dataEntrySuccessMessage: '',
      dataEntryErrorMessage: '',
      pageLoadErrorMessage: '',
      islands: [],
      uiState: {
          logoUploadState: '',
          logoUploadMsg: '',
          logoUploadingProgress: 0,
          basicInfoState: '',
          basicInfoMsg: '',
          registrationUploadState: '',
          registrationUploadMsg: '',
          registrationUploadingProgress: 0
        },

      uploader: null,
      wordCounterIntroduction: 0,
      introductionLimitWarning: 'Min limit not reached',
      wordCounterProductsAndServices: 0,
      productsLimitWarning: 'Min limit not reached',
      wordCounterCSR: 0,
      csrLimitWarning: 'Min limit not reached',
      submitEnabled: true,
      limits : {
        intro : {
          min: 100,
          max: 120
        },
        products: {
          min: 60,
          max: 80
        },
        csr: {
          min: 50,
          max: 200
        }
      },
      classes : {
        introductionLimitWarningClass: 'italic pull-right warn',
        productsLimitWarningClass: 'italic pull-right warn',
        csrLimitWarningClass: 'italic pull-right warn',
      }
    },
    methods: {
      getCompany() {
        let thisComponent = this

        axios.get('{{ url("admin/get-company")}}/' + thisComponent.companyId)
          .then(function (response) {

            thisComponent.dataModel = response.data
            for(i=0; i<response.data.subsidiaries.length; i++) {
              let data = {
                count: i+1,
                subsidiary: response.data.subsidiaries[i].name
              }
              thisComponent.subsidiaries.push(data)
            }
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.uiState.basicInfoState = 'error'
                thisComponent.uiState.basicInfoMsg = error.response.data.errors[0].detail
              }
              $('#loader').hide()
              thisComponent.submitEnabled = true
            } else {
              thisComponent.uiState.basicInfoState = 'error'
              thisComponent.uiState.basicInfoMsg = 'Unknown error.'
              $('#loader').hide()
              thisComponent.submitEnabled = true
            }
          })

      },

      // adds a row in subsidiaries
      addSubsidiaryRow ($event) {
        event.preventDefault()

        let component = this
        let counter = component.subsidiaries.length
        console.log(counter)
        component.subsidiaries.push({count: counter + 1, subsidiary: ''});
      },
      // remove subsidiary row
      removeSubsidiaryRow (row, $event) {
        event.preventDefault()

        let component = this
        let counter = component.subsidiaries.length

        if (counter > 1) {
          component.subsidiaries.splice(_.findIndex(component.subsidiaries, function(o) { return o.count == row.count }), 
               1);
        }
        
      },
      
      // register the company
      registerCompany () {
        let thisComponent = this
        let data = thisComponent.dataModel
        data.subsidiaries = thisComponent.subsidiaries

        // if (thisComponent.introductionLimitWarning !== 'OK') {
        //   thisComponent.uiState.basicInfoState = 'error'
        //   thisComponent.uiState.basicInfoMsg = 'Minimun or Maximum word requirement for Company Introduction not met'
        //   return
        // }

        // if (thisComponent.productsLimitWarning !== 'OK') {
        //   thisComponent.uiState.basicInfoState = 'error'
        //   thisComponent.uiState.basicInfoMsg = 'Minimun or Maximum word requirement for Products & Services not met'
        //   return
        // }

        // if (thisComponent.csrLimitWarning !== 'OK') {
        //   thisComponent.uiState.basicInfoState = 'error'
        //   thisComponent.uiState.basicInfoMsg = 'Minimun or Maximum word requirement for History of Business Success, Innovativeness and CSR Innitiatives not met'
        //   return
        // }

        $('#loader').show()
        thisComponent.submitEnabled = false

        // set response in datamodel

        axios.put('{{ url("admin/company")}}/' + data.id, data)
          .then(function (response) {

            thisComponent.uiState.basicInfoState = 'success'
            thisComponent.uiState.basicInfoMsg = 'Entry for ' + data.company_name + ' has been updated successfully.'
            $('.remove-button').attr('disabled', true);
            $('#loader').hide()
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.uiState.basicInfoState = 'error'
                thisComponent.uiState.basicInfoMsg = error.response.data.errors[0].detail
              }
              $('#loader').hide()
              thisComponent.submitEnabled = true
            } else {
              thisComponent.uiState.basicInfoState = 'error'
              thisComponent.uiState.basicInfoMsg = 'Unknown error.'
              $('#loader').hide()
              thisComponent.submitEnabled = true
            }
          })

      },
      confirmDeletePhoto () { },
      // delete uploaded logo
      deleteLogo () {
        let thisComponent = this

        axios.delete('company-logo/' + thisComponent.dataModel.company_logo)
          .then(function (response) {
           $('#modal-confirm-delete-logo').modal('hide')

            thisComponent.uiState.basicInfoState = 'success'
            thisComponent.uiState.basicInfoMsg = 'Logo deleted successfully.'
            thisComponent.companyLogo = ''
            thisComponent.dataModel.company_logo = ''
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.dataEntryErrorMessage = error.response.data.errors[0].detail
              }
            } else {
              thisComponent.uiState.basicInfoState = 'error'
              thisComponent.uiState.basicInfoMsg = 'Unknown error.'
            }
          })
      },
      // delete uploaded registration certificate
      deleteRegistration () {
        let thisComponent = this

        axios.delete('company-registration/' + thisComponent.dataModel.company_registration)
          .then(function (response) {
           $('#modal-confirm-delete-registration').modal('hide')

            thisComponent.uiState.basicInfoState = 'success'
            thisComponent.uiState.basicInfoMsg = 'Registration certificate deleted successfully.'
            thisComponent.companyRegistration = ''
            thisComponent.dataModel.company_registration = ''
          })
          .catch(function (error) {
            if (error.response) {
              if (error.response.data && error.response.data.errors) {
                thisComponent.dataEntryErrorMessage = error.response.data.errors[0].detail
              }
            } else {
              thisComponent.uiState.basicInfoState = 'error'
              thisComponent.uiState.basicInfoMsg = 'Unknown error.'
            }
          })
      },
      
      // word counter
      wordCount(theObject, counter) { 
        var regex = /\s+/gi;

        let thisComponent = this
        if(counter === 'introduction') {
          thisComponent.wordCounterIntroduction = theObject.trim().replace(regex, ' ').split(' ').length

          if (thisComponent.wordCounterIntroduction < thisComponent.limits.intro.min) {
            thisComponent.introductionLimitWarning = 'Min limit not reached'
            thisComponent.classes.introductionLimitWarningClass = 'italic pull-right warn'
          } else if (thisComponent.wordCounterIntroduction >= thisComponent.limits.intro.min && thisComponent.wordCounterIntroduction <= thisComponent.limits.intro.max) {
            thisComponent.introductionLimitWarning = 'OK'
            thisComponent.classes.introductionLimitWarningClass = 'italic pull-right ok'
          } else {
            thisComponent.introductionLimitWarning = 'Max limit exceeded'
            thisComponent.classes.introductionLimitWarningClass = 'italic pull-right warn'
          }
        } else if (counter === 'csr') {
          thisComponent.wordCounterCSR = theObject.trim().replace(regex, ' ').split(' ').length

          if (thisComponent.wordCounterCSR < thisComponent.limits.csr.min) {
            thisComponent.csrLimitWarning = 'Min limit not reached'
            thisComponent.classes.csrLimitWarningClass = 'italic pull-right warn'
          } else if (thisComponent.wordCounterCSR >= thisComponent.limits.csr.min && thisComponent.wordCounterCSR <= thisComponent.limits.csr.max) {
            thisComponent.csrLimitWarning = 'OK'
            thisComponent.classes.csrLimitWarningClass = 'italic pull-right ok'
          } else {
            thisComponent.csrLimitWarning = 'Max limit exceeded'
            thisComponent.classes.csrLimitWarningClass = 'italic pull-right warn'
          }
        } else if (counter === 'products_and_services') {
          thisComponent.wordCounterProductsAndServices = theObject.trim().replace(regex, ' ').split(' ').length

          if (thisComponent.wordCounterProductsAndServices < thisComponent.limits.products.min) {
            thisComponent.productsLimitWarning = 'Min limit not reached'
            thisComponent.classes.productsLimitWarningClass = 'italic pull-right warn'
          } else if (thisComponent.wordCounterProductsAndServices >= thisComponent.limits.products.min && thisComponent.wordCounterProductsAndServices <= thisComponent.limits.products.max) {
            thisComponent.productsLimitWarning = 'OK'
            thisComponent.classes.productsLimitWarningClass = 'italic pull-right ok'
          } else {
            thisComponent.productsLimitWarning = 'Max limit exceeded'
            thisComponent.classes.productsLimitWarningClass = 'italic pull-right warn'
          }
        }
      }
    },

    created () { },

    mounted () {
      let thisComponent = this

      this.getCompany()

     // this.setUpLogoUploader()
     // this.setUpRegistrationUploader()

      $('#registration_date').datetimepicker({
        format: 'DD-MMM-YYYY',
      //  maxDate: moment().add(-5, 'years'),
        useCurrent: false,
        defaultDate: '2012-12-31',
        maxDate: '2012-12-31'
      }).on('blur', function(){
        thisComponent.dataModel.registration_date = $(this).val()
      })



    },

    updated () {
    //  this.setUpLogoUploader()
     // this.setUpRegistrationUploader()
    }
});


</script>


@stop


@section('styles')
@parent
<style scoped>
.required_field {
  color: #dd4b39;
}
.robotic {
  display: none;
}
hr {
  margin-bottom: 8px;
}

.warn {
  font-size: 12px;
  color: #dd4b39;
}

.ok {
  font-size: 12px;
  color: #008B00;
}

/*@media (max-width: 768px) {
  .subsidiary-count2, .subsidiary-remove-button2, .subsidiary-add-buttons {
    text-align: left !important;
    float: left;
    padding-left: 0 !important;
  }
}*/

</style>
@stop