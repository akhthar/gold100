<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//CMS
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('admin/dashboard', 'cms\HomeController@index');
Route::get('admin', 'cms\HomeController@index');

Route::get('cms', 'cms\HomeController@index');

Route::get('admin/requests', 'cms\HomeController@getRequests');

Route::delete('admin/request/{id}', 'cms\HomeController@deleteRequest');
// Route::delete('admin/request/reject/{id}', 'cms\HomeController@deleteRequest');

Route::get('admin/new-requests', 'cms\HomeController@getNewRequests');

Route::get('admin/accept-request/{id}', 'cms\HomeController@acceptRequest');
Route::get('admin/reject-request/{id}', 'cms\HomeController@rejectRequest');

Route::get('admin/revoke-request/{id}', 'cms\HomeController@revokeRequest');

Route::get('admin/accepted-companies', 'cms\HomeController@registeredCompanies');

Route::get('admin/companies', 'cms\HomeController@getRegisteredCompanies');

Route::get('admin/rejected', 'cms\HomeController@getRejected');
Route::get('admin/rejected-companies', 'cms\HomeController@getRejectedCompanies');


// user routes
Route::get('admin/users', 'cms\UserController@index');

Route::post('admin/users', 'cms\UserController@createUser');

Route::delete('admin/users/{id}', 'cms\UserController@deleteUser');

Route::post('admin/users/change-password', 'cms\UserController@changePassword');

Route::get('admin/reset-password/{id}', 'cms\UserController@resetPassword');

Route::get('admin/get-users', 'cms\UserController@getUsers');

Route::get('admin/downloadInfo', 'cms\HomeController@downloadComapniesCSV');


//
Route::get('admin/company/{id}', 'cms\HomeController@getCompany');
Route::get('admin/get-company/{id}', 'cms\HomeController@getCompanyInfo');
Route::put('admin/company/{id}', 'cms\HomeController@update');



// public routes
Route::get('/', 'HomeController@index');


Route::post('/register-company', 'HomeController@register');

Route::post('/company-logo', 'HomeController@uploadCompanyLogo');
Route::post('/company-registration', 'HomeController@uploadCompanyRegistration');

Route::delete('/company-logo/{id}', 'HomeController@deleteCompanyLogo');
Route::delete('/company-registration/{id}', 'HomeController@deleteCompanyRegistration');




Route::get('admin/downloadCSV', 'cms\HomeController@downloadCSV');

