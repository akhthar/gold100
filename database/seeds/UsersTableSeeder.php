<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ts = new DateTime();

        DB::table('users')->insert([
            'name' => 'Ali Akhthar',
            'username' => 'akhthar',
            'email' => 'akhthar@laniakea.mv',
            'user_type' => 1,
            'password' => Hash::make('felina@14'),
            'is_active' => true,
            'created_at' => $ts,
            'updated_at' => $ts,
        ]);
        
    }
}
