<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {

// company_name
// registration_number
// managing_director
// registration_date
// number_of_employees
// revenue_currency_2017
// annual_gross_revenue_2017
// revenue_currency_2016
// annual_gross_revenue_2016
// revenue_currency_2015
// annual_gross_revenue_2015
// industry
// introduction
// products_and_services
// business_success_and_innovativeness
// address
// contact_number
// email
// website
// contact_person
// contact_person_number
// contact_email

            $table->increments('id');
            $table->string('company_name', 250);
            $table->string('registration_number', 250);
            $table->string('managing_director', 250);
            $table->datetime('registration_date');
            $table->integer('number_of_employees');
            $table->string('revenue_currency_2017', 10);
            $table->string('annual_gross_revenue_2017', 100);
            $table->string('revenue_currency_2016', 10);
            $table->string('annual_gross_revenue_2016', 100);
            $table->string('revenue_currency_2015', 10);
            $table->string('annual_gross_revenue_2015', 100);
            $table->string('industry', 100);
            // $table->string('ownership', 250);
            // $table->string('subsidiaries', 1000);
            $table->text('introduction');
            $table->text('products_and_services');
            $table->text('business_success_and_innovativeness');
            $table->string('address', 250);
            $table->string('contact_number', 250);
            $table->string('email', 250);
            $table->string('website', 250);

            $table->string('contact_person', 250);
            $table->string('contact_person_number', 250);
            $table->string('contact_email', 250);
            
            $table->string('client_ip_address')->nullable();
            $table->string('company_logo', 250)->nullable();
            $table->string('company_registration', 250)->nullable();
            $table->boolean('is_registered')->default(false);
            $table->integer('registered_by')
                    ->refrences('users')
                    ->on('id')->nullable();
            $table->boolean('is_rejected')->default(false);
            $table->integer('rejected_by')
                    ->refrences('users')
                    ->on('id')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
