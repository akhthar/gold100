<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
    //
    // public function island() {
    // 	return $this->belongsTo('App\Island');
    // }

    // public function toArray() {
    // 	return [
    //         'id' => $this->id,
    //         'property_name' => $this->property_name,
    //         'island' => $this->island->atoll->abbr_en . '. ' . $this->island->name_en,
    //         'distance_from_airport_by_ferry' => $this->distance_from_airport_by_ferry,
    //         'distance_from_airport_by_speedboat' => $this->distance_from_airport_by_speedboat,
    //         'distance_from_airport_by_aeroplane' => $this->distance_from_airport_by_aeroplane,
    //         'distance_from_airport_by_car' => $this->distance_from_airport_by_car,
    //         'about' => $this->about,
    //         'number_of_rooms' => $this->number_of_rooms,
    //         'contact_number' => $this->contact_number,
    //         'contact_person' => $this->contact_person,
    //         'email' => $this->email,
    //         'fax_number' => $this->fax_number,
    //         'facebook' => $this->facebook,
    //         'website' => $this->website,
    //         'client_ip_address' => $this->client_ip_address,
    //         'magazine_photo' => $this->magazine_photo,
    //         'is_registered' => $this->is_registered ? true : false
    //     ];
    // }
}
