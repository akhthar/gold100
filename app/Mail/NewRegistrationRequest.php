<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRegistrationRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $propertyName;
    public $clientIPAddress;
    public $requestedTime;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($propertyName, $clientIPAddress, $requestedTime)
    {
        $this->propertyName = $propertyName;
        $this->clientIPAddress =  $clientIPAddress;
        $this->requestedTime = $requestedTime;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Guesthouses Conference Registration Request')
                    ->view('emails.new-request');
    }
}
