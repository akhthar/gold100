<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RawMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $recipientName;
    public $title;
    public $messageBody;

    // public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipientName, $title, $messageBody)
    {
        $this->recipientName = $recipientName;
        $this->title = $title;
        $this->messageBody = $messageBody; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->title)
                    ->view('emails.raw-message');
    }
}
