<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMeetingRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $recipientName;

    // public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipientName)
    {
        $this->recipientName = $recipientName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New meeting request at "CONNECT" Conference Management System of TTM')
                    ->view('emails.meeting-request');
    }
}
