<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $loginId;

    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($loginId, $password)
    {
        $this->loginId = $loginId;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Guesthouses Conference CMS Account')
                    ->view('emails.new-account');
    }
}
