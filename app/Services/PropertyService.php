<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

use App\Exceptions\AuthenticationFailedException;
use App\Exceptions\AuthorisationFailedException;
use App\Exceptions\InvalidOperationException;
use App\Exceptions\EntityNotFoundException;

use Illuminate\Support\Facades\Mail;

use \DateTime;

use Illuminate\Support\Facades\Notification;
use App\Notifications\Gold100NewRegistration;
use App\Notifications\Gold100InformationFormSubmission;
use App\Notifications\Gold100Accepted;
use App\Notifications\Gold100Rejected;
// use App\Notifications\NewRegistrationRequest;

use App\Pdf\AttendeeAccessCardPdf;

use App\Session;

use App\User;
use App\Company;
use App\Subsidiary;
// use App\Models\ReadNotification;
// use App\Models\ActionTakenNotification;

use App\Exceptions\ValidationException;

class PropertyService
{
	private $authenticationService;

	/**
	 * Constructor.
	 */
	public function __construct(AuthenticationService $authenticationService) {
		$this->authenticationService = $authenticationService;
	}

	public function getClient($id) {
		return Company::find($id);
	}

	public function getPropertyByUniqueId($uniqueId) {
		return Company::where('magazine_photo', 'like', '%'.$uniqueId.'%')->get()->first();
	}

	public function getNewCompanies($data) {

		return Company::with(['subsidiaries'])->where('is_registered', false)
						->where('deleted_at', null)
						->where('is_rejected', false)
						->orderBy('created_at', 'desc')
						->get();

		
	}

	public function getRejectedCompanies($data) {

		return Company::with(['subsidiaries'])
						
						->where('is_rejected', true)
						->orderBy('created_at', 'desc')
						->get();

		
	}

	public function getRegisteredCompanies($params=null) {

		$company_name = array_get($params, 'company_name', '');

		$orderBy = array_get($params, 'order_by', 'created_at');

		$orderDir = array_get($params, 'order_direction', 'asc');

		$page =1;//= array_get($params, 'page', 1);

		$limit =300;//= array_get($params, 'limit', 300);

		// $query = Company::where('is_registered', true)
		// 				->where('is_rejected', false);

		if (!empty($company_name)) {
			$query->where('company_name', 'like', '%'.$company_name.'%');
		}

		$query = DB::table('companies')
					//->join('islands', 'companies.island_id', 'islands.id')
					//->leftJoin('atolls', 'islands.atoll_id', 'atolls.id')
					->select('companies.id')
					->where('companies.is_registered', true);
					// ->where('companies.deleted_at', null);

		if (!empty($company_name)) {
			$query->where('companies.company_name', 'like', '%'.$company_name.'%');
		}


			$orderByColumn = 'companies.' . $orderBy;


		$companies = $query->orderBy($orderByColumn, $orderDir)
			->skip(($page - 1) * $limit)
			->take($limit)
			->get();

		$companiesArr = [];
		foreach ($companies as $key => $c) {
			$company = Company::find($c->id)->load('subsidiaries');
			$companiesArr[] = $company;
		}

		return $companiesArr;

	}

	public function createClient($data, $request) {
		return DB::transaction(function() use ($data, $request) {
			// $this->authenticationService->assertRole('organiser');

			$client = $this->createClientFromData($data, $request);
		//	echo '<pre>';

			
			if($client->save()) {

				// $subsidiaries = $this->createSubsidiariesFromData($data, $client);
				$subsidiaries = array_get($data, 'subsidiaries');
				if(count($subsidiaries) > 0) {
					foreach($subsidiaries as $k => $sub) {
						$subsidiary = new Subsidiary();
						$subsidiary->name = $sub['subsidiary'];
						$subsidiary->company_id = $client->id;
						$subsidiary->save();
					}
					

				}
				
				// email to the client
				// $client->notify(new Gold100InformationFormSubmission($client));
				
	        	$users = User::where('is_active', true)->get();
	        	// Notification::send($users, new Gold100NewRegistration($client, 'Corporate Maldives'));
	        	// Mail::to('editor@corporate.mv')->queue(new NewRegistrationRequest($client->company_name, $client->client_ip_address, $client->created_at));
			}

			$request->session()->flush();
			$request->session()->regenerate();

			$client = $this->getClient($client->id);

			return $client;
		});
	}

	public function updateClient($data, $request) {
		return DB::transaction(function() use ($data, $request) {
			// $this->authenticationService->assertRole('organiser');

			$client = $this->updateClientFromData($data, $request);
		//	echo '<pre>';

			
			if($client->save()) {

				// $subsidiaries = $this->createSubsidiariesFromData($data, $client);
				// $subsidiaries = array_get($data, 'subsidiaries');
				// if(count($subsidiaries) > 0) {
				// 	foreach($subsidiaries as $k => $sub) {
				// 		//$subsidiary = new Subsidiary();
				// 		$subsidiary->name = $sub['subsidiary'];
				// 		$subsidiary->company_id = $client->id;
				// 		$subsidiary->save();
				// 	}
					

				// }
			// 	print_r($client);
			// die();
				// email to the client
				// $client->notify(new Gold100InformationFormSubmission($client));
				
	   //      	$users = User::where('is_active', true)->get();
	   //      	Notification::send($users, new Gold100NewRegistration($client, 'Corporate Maldives'));
	        	// Mail::to('editor@corporate.mv')->queue(new NewRegistrationRequest($client->company_name, $client->client_ip_address, $client->created_at));
			}

			// $request->session()->flush();
			// $request->session()->regenerate();

			$client = $this->getClient($client->id);

			return $client;
		});
	}

	public function addCompanyLogo(Request $request, \Illuminate\Http\UploadedFile $file, $isCoverPhoto = false, $isLogo = false) {
		
		// use session id as the file name for the photo
		$sessionId = $request->session()->getId();

		// Check if uploaded photo is valid.

		if (!$file->isValid()) {
			return [
				'success' => false,
				'error' => 'There was error uploading your logo. Please try again later.'
			];
        }

        // check file size to limit it to 2mb
        if($file->getClientSize() > 2097152) {
        	return [
				'success' => false,
				'error' => 'File size must be less than 2MB.'
			];
        }

        // Store file to profile photos.

        // $filename = $file->store('', 'magazine_photos');
        $filename  = $sessionId . '.' . $file->getClientOriginalExtension();

        $path = public_path('company_logos/' . $filename);
        Image::make($file->getRealPath())->save($path);

       	// Done.

        return ['success' => true, 'logo' => $filename];
	}

	public function addCompanyRegistration(Request $request, \Illuminate\Http\UploadedFile $file, $isCoverPhoto = false, $isLogo = false) {
		
		// use session id as the file name for the photo
		$sessionId = $request->session()->getId();

		// Check if uploaded photo is valid.

		if (!$file->isValid()) {
			return [
				'success' => false,
				'error' => 'There was error uploading your registration. Please try again later.'
			];
        }

        // check file size to limit it to 2mb
        if($file->getClientSize() > 2097152) {
        	return [
				'success' => false,
				'error' => 'File size must be less than 2MB.'
			];
        }

        // Store file to profile photos.

        // $filename = $file->store('', 'magazine_photos');
        $filename  = $sessionId . '.' . $file->getClientOriginalExtension();

        $path = public_path('company_registrations/' . $filename);
        Image::make($file->getRealPath())->save($path);

       	// Done.

        return ['success' => true, 'registration' => $filename];
	}

	public function deleteCompanyLogo($id) {
		// Get authenticated participant.

		
		$file = explode('.', $id);
		$fileName = $file[0];
		$ext = $file[1];
		$thumFileName = $fileName . '_thumb' . $ext;

		Storage::disk('company_logos')->delete($id);
		Storage::disk('company_logos')->delete($thumFileName);

		// Done.

		return true;
	}

	public function deleteCompanyRegistration($id) {
		// Get authenticated participant.

		
		$file = explode('.', $id);
		$fileName = $file[0];
		$ext = $file[1];
		$thumFileName = $fileName . '_thumb' . $ext;

		Storage::disk('company_registrations')->delete($id);
		Storage::disk('company_registrations')->delete($thumFileName);

		// Done.

		return true;
	}

	public function acceptRegistrationRequest($id, Request $request) {
		return DB::transaction(function() use ($id, $request) {
			// $this->authenticationService->assertRole('organiser');

			// $client = $this->createClientFromData($data, $request);
			$client = $this->getClient($id);
			$client->is_registered = true;
			$client->registered_by = Auth()->user()->id;

			$client->save();
				// $title = 'Gold100 Registration Confirmation';
	   //      	$messageBody = 'Your request to register for GOLD100 has been accepted. Our staff may contact you on further instructions.';

	        	// Mail::to($client->email)->queue(new RawMessage($client->company_name, $title, $messageBody));
	        	// $client->notify(new Gold100Accepted($client));
			// }

			$client = $this->getClient($client->id);

			return $client;
		});
    }

    public function rejectRegistrationRequest($id, Request $request) {
		return DB::transaction(function() use ($id, $request) {
			// $this->authenticationService->assertRole('organiser');

			// $client = $this->createClientFromData($data, $request);
			$client = $this->getClient($id);
			$client->is_registered = false;
			$client->is_rejected = true;
			$client->rejected_by = Auth()->user()->id;

			$client->save();
			// if($client->save()) {
				// $title = 'Gold100 Registration Rejectd';
	        	// $messageBody = 'Your request to register for GOLD100 has been rejected. Please contact our staff for information.';

	        	// $client->notify(new Gold100Rejected($client));

	        	// Mail::to($client->email)->queue(new RawMessage($client->company_name, $title, $messageBody));
			// }

			$client = $this->getClient($client->id);

			return $client;
		});
    }

    private function acceptRequest($client) {

    }


	private function createClientFromData($data, $request) {
		// echo '<pre>';
		// print_r($request);
		// echo '</pre>';
		$sessionId = $request->session()->getId();

		// die();
		

		$company_name = array_get($data, 'company_name', '');
		$registration_number = array_get($data, 'registration_number', '');
		$managing_director = array_get($data, 'managing_director', '');
		$registration_date = array_get($data, 'registration_date', '');
		$number_of_employees = array_get($data, 'number_of_employees', '');
		$revenue_currency_2017 = array_get($data, 'revenue_currency_2018', '');
		$annual_gross_revenue_2017 = array_get($data, 'annual_gross_revenue_2018', '');
		$revenue_currency_2016 = array_get($data, 'revenue_currency_2017', '');
		$annual_gross_revenue_2016 = array_get($data, 'annual_gross_revenue_2017', '');
		$revenue_currency_2015 = array_get($data, 'revenue_currency_2016', '');
		$annual_gross_revenue_2015 = array_get($data, 'annual_gross_revenue_2016', '');
		$industry = array_get($data, 'industry', '');
		// $ownership = array_get($data, 'ownership', '');
		$subsidiaries = array_get($data, 'subsidiaries', '');
		$introduction = array_get($data, 'introduction', '');
		$products_and_services = array_get($data, 'products_and_services', '');
		$business_success_and_innovativeness = array_get($data, 'business_success_and_innovativeness', '');
		$address = array_get($data, 'address', '');
		$contact_number = array_get($data, 'contact_number', '');
		$email = array_get($data, 'email', '');
		$website = array_get($data, 'website', '');
		$contact_person = array_get($data, 'contact_person', '');
		$contact_person_number = array_get($data, 'contact_person_number', '');
		$contact_email = array_get($data, 'contact_email', '');
		$company_logo = array_get($data, 'company_logo', '');
		$company_registration = array_get($data, 'company_registration', '');

		// honey trap check
		$robotTest = array_get($data, 'rob', '');
		if(!empty($robotTest)) {
			throw new InvalidOperationException('ROBOT not allowed.');
		}

		if (empty($company_name)) {
			throw new ValidationException('Company Name is required.');
		}

		if (empty($registration_number) || $registration_number == '') {
			throw new ValidationException('Registration Number is required.');
		}

		if (empty($managing_director) || $managing_director == '') {
			throw new ValidationException('Managing Director is required.');
		}
		
		if (empty($registration_date)) {
			throw new ValidationException('Registration date is required.');
		} else {
			$registration_date = (new DateTime($registration_date))->format('Y-m-d');
		}

		if (empty($number_of_employees)) {
			throw new ValidationException('Number of employees is required.');
		}

		if (empty($revenue_currency_2017)) {
			throw new ValidationException('Revenue currency for 2022 is required.');
		}

		if (empty($annual_gross_revenue_2017)) {
			throw new ValidationException('Annual gross revenue for 2022 is required.');
		}

		if (empty($revenue_currency_2016)) {
			throw new ValidationException('Revenue currency for 2021 is required.');
		}

		if (empty($annual_gross_revenue_2016)) {
			throw new ValidationException('Annual gross revenue for 2021 is required.');
		}

		if (empty($revenue_currency_2015)) {
			throw new ValidationException('Revenue currency for 2020 is required.');
		}

		if (empty($annual_gross_revenue_2015)) {
			throw new ValidationException('Annual gross revenue for 2020 is required.');
		}

		if (empty($industry)) {
			throw new ValidationException('Industry is required.');
		}

		// if (empty($ownership)) {
		// 	throw new ValidationException('Ownership the company is required.');
		// }

		// if (empty($subsidiaries)) {
		// 	throw new ValidationException('Subsidiaries the company is required.');
		// }

		if (empty($introduction)) { // || str_word_count($introduction) < 100) {
			throw new ValidationException('Introduction of the company must be at least 100 words.');
		}

		// if (str_word_count($introduction) > 120) {
		// 	throw new ValidationException('Introduction of the company must not be more than 120 words.');
		// }

		if (empty($products_and_services)) { //  || str_word_count($products_and_services) < 60) {
			throw new ValidationException('Information about the products and services must at least 60 words.');
		}

		// if (str_word_count($products_and_services) > 80) {
		// 	throw new ValidationException('Introduction of the products and services must not be more than 80 words.');
		// }

		if (empty($business_success_and_innovativeness)) { //  || str_word_count($business_success_and_innovativeness) < 50) {
			throw new ValidationException('Information about the businness success, innovativeness and CSR initiatives must be atleast 50 words.');
		}

		// if (str_word_count($business_success_and_innovativeness) > 200) {
		// 	throw new ValidationException('Information about the businness success, innovativeness and CSR initiatives must not be more than 200 words.');
		// }

		if (empty($address)) {
			throw new ValidationException('Address is required.');
		}

		if(empty($contact_number)) {
			throw new ValidationException('Contact number is required.');
		}
		
		if (empty($email)) {
			throw new ValidationException('Business email address is required.');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // throw new ValidationException('Business email is not valid.');
        }

		if (empty($website)) {
			throw new ValidationException('Website address is required.');
		}

		
		if(empty($contact_person)) {
			throw new ValidationException('Contact name is required.');
		}

		if(empty($contact_person_number)) {
			throw new ValidationException('Contact name number is required.');
		}

		if(empty($contact_email)) {
			throw new ValidationException('Contact email is required.');
		}

		if (!filter_var($contact_email, FILTER_VALIDATE_EMAIL)) {
            // throw new ValidationException('Contact email is not valid.');
        }

		if (empty($company_logo)) {
			throw new ValidationException('Company logo is required.');
		}

		if (empty($company_registration)) {
			throw new ValidationException('Copy of company registration is required.');
		}

		// get the reCaptcha value
        // $captcha = trim(array_get($data, 'g_recaptcha_response', '')) != '' ? trim(array_get($data, 'g_recaptcha_response', '')) : null;

        // validate reCaptcha... 
        // this should only be enabled on the production server because the secret key would be registered to the production server
        // $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeeDioUAAAAAHdQiQBbOAvN4T_ZQz7-cD7B2iD7&response=" . $captcha . "&remoteip=" . $request->ip()));
        // echo '<pre>';
        //  echo 'secret->' . env("GOOGLE_RECAPTCHA_SECRET") . '<-';
        // print_r($response);
        // echo '</pre>';
        // if (!$response->success) {
        //    throw new ValidationException('reCaptcha validation failed. Please try again.');
        // }
		

		$company = new Company();
		$company->company_name = $company_name;
		$company->registration_number = $registration_number;
		$company->managing_director = $managing_director;
		$company->registration_date = $registration_date;
		$company->number_of_employees = $number_of_employees;
		$company->revenue_currency_2017 = $revenue_currency_2017;
		$company->annual_gross_revenue_2017 = $annual_gross_revenue_2017;
		$company->revenue_currency_2016 = $revenue_currency_2016;
		$company->annual_gross_revenue_2016 = $annual_gross_revenue_2016;
		$company->revenue_currency_2015 = $revenue_currency_2015;
		$company->annual_gross_revenue_2015 = $annual_gross_revenue_2015;
		$company->industry = $industry;
		// $company->ownership = $ownership;
		// $company->subsidiaries = $subsidiaries;
		$company->introduction = $introduction;
		$company->products_and_services = $products_and_services;
		$company->business_success_and_innovativeness = $business_success_and_innovativeness;
		$company->address = $address;
		$company->contact_number = $contact_number;
		$company->email = $email;
		$company->website = $website;
		$company->contact_person = $contact_person;
		$company->contact_person_number = $contact_person_number;
		$company->contact_email = $contact_email;
		$company->company_logo = $company_logo;
		$company->company_registration = $company_registration;
		$company->client_ip_address = $request->ip();

		// company_name
// registration_number
// managing_director
// registration_date
// number_of_employees
// revenue_currency_2017
// annual_gross_revenue_2017
// revenue_currency_2016
// annual_gross_revenue_2016
// revenue_currency_2015
// annual_gross_revenue_2015
// industry
// introduction
// products_and_services
// business_success_and_innovativeness
// address
// contact_number
// email
// website
// contact_person
// contact_person_number
// contact_email
		
		return $company;

	}


	private function updateClientFromData($data, $request) {

		$id = array_get($data, 'id');
		$company = Company::find($id);
		if(empty($company)) {
			throw new EntityNotFoundException("Requested company was not found.");
		}
		

		$company_name = array_get($data, 'company_name', '');
		$registration_number = array_get($data, 'registration_number', '');
		$managing_director = array_get($data, 'managing_director', '');
		$registration_date = array_get($data, 'registration_date', '');
		$number_of_employees = array_get($data, 'number_of_employees', '');
		$revenue_currency_2017 = array_get($data, 'revenue_currency_2017', '');
		$annual_gross_revenue_2017 = array_get($data, 'annual_gross_revenue_2017', '');
		$revenue_currency_2016 = array_get($data, 'revenue_currency_2016', '');
		$annual_gross_revenue_2016 = array_get($data, 'annual_gross_revenue_2016', '');
		$revenue_currency_2015 = array_get($data, 'revenue_currency_2015', '');
		$annual_gross_revenue_2015 = array_get($data, 'annual_gross_revenue_2015', '');
		$industry = array_get($data, 'industry', '');
		$introduction = array_get($data, 'introduction', '');
		$products_and_services = array_get($data, 'products_and_services', '');
		$business_success_and_innovativeness = array_get($data, 'business_success_and_innovativeness', '');
		$address = array_get($data, 'address', '');
		$contact_number = array_get($data, 'contact_number', '');
		$email = array_get($data, 'email', '');
		$website = array_get($data, 'website', '');
		$contact_person = array_get($data, 'contact_person', '');
		$contact_person_number = array_get($data, 'contact_person_number', '');
		$contact_email = array_get($data, 'contact_email', '');

		$robotTest = array_get($data, 'rob', '');
		if(!empty($robotTest)) {
			throw new InvalidOperationException('Damned you robot. LOL');
		}

		if (empty($company_name)) {
			throw new ValidationException('Company Name is required.');
		}

		if (empty($registration_number) || $registration_number == '') {
			throw new ValidationException('Registration Number is required.');
		}

		if (empty($managing_director) || $managing_director == '') {
			throw new ValidationException('Managing Director is required.');
		}
		
		if (empty($registration_date)) {
			throw new ValidationException('Registration date is required.');
		} else {
			$registration_date = (new DateTime($registration_date))->format('Y-m-d');
		}

		if (empty($number_of_employees)) {
			throw new ValidationException('Number of employees is required.');
		}

		if (empty($revenue_currency_2017)) {
			throw new ValidationException('Revenue currency for 2017 is required.');
		}

		if (empty($annual_gross_revenue_2017)) {
			throw new ValidationException('Annual gross revenue for 2017 is required.');
		}

		if (empty($revenue_currency_2016)) {
			throw new ValidationException('Revenue currency for 2016 is required.');
		}

		if (empty($annual_gross_revenue_2016)) {
			throw new ValidationException('Annual gross revenue for 2016 is required.');
		}

		if (empty($revenue_currency_2015)) {
			throw new ValidationException('Revenue currency for 2015 is required.');
		}

		if (empty($annual_gross_revenue_2015)) {
			throw new ValidationException('Annual gross revenue for 2015 is required.');
		}

		if (empty($industry)) {
			throw new ValidationException('Industry is required.');
		}

		if (empty($address)) {
			throw new ValidationException('Address is required.');
		}

		if(empty($contact_number)) {
			throw new ValidationException('Contact number is required.');
		}
		
		if (empty($email)) {
			throw new ValidationException('Business email address is required.');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('Business email is not valid.');
        }

		if (empty($website)) {
			throw new ValidationException('Website address is required.');
		}

		
		if(empty($contact_person)) {
			throw new ValidationException('Contact name is required.');
		}

		if(empty($contact_person_number)) {
			throw new ValidationException('Contact name number is required.');
		}

		if(empty($contact_email)) {
			throw new ValidationException('Contact email is required.');
		}

		if (!filter_var($contact_email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('Contact email is not valid.');
        }

		// $company = new Company();
		$company->company_name = $company_name;
		$company->registration_number = $registration_number;
		$company->managing_director = $managing_director;
		$company->registration_date = $registration_date;
		$company->number_of_employees = $number_of_employees;
		$company->revenue_currency_2017 = $revenue_currency_2017;
		$company->annual_gross_revenue_2017 = $annual_gross_revenue_2017;
		$company->revenue_currency_2016 = $revenue_currency_2016;
		$company->annual_gross_revenue_2016 = $annual_gross_revenue_2016;
		$company->revenue_currency_2015 = $revenue_currency_2015;
		$company->annual_gross_revenue_2015 = $annual_gross_revenue_2015;
		$company->industry = $industry;
		$company->introduction = $introduction;
		$company->products_and_services = $products_and_services;
		$company->business_success_and_innovativeness = $business_success_and_innovativeness;
		$company->address = $address;
		$company->contact_number = $contact_number;
		$company->email = $email;
		$company->website = $website;
		$company->contact_person = $contact_person;
		$company->contact_person_number = $contact_person_number;
		$company->contact_email = $contact_email;

		$company->is_registered = false;
		$company->is_rejected = false;

		$company->created_at = (new \DateTime())->format('Y-m-d H:i:s');
		
		return $company;
	}



	public function downloadCSV() {
		$sql = "select company_name, ceo_managing_director,
				year(year_started) as year_started,
				employment,
				revenue,
				industry,
				ownership,
				subsidiaries,
				introduction,
				products_and_services,
				business_success_and_innovativeness,
				address,
				contact_number,
				email,
				website from companies where is_registered = 1 and deleted_at is null order by company_name";

		$records = DB::select($sql);
		$result = [];

		foreach ($records as $r) {
			$result[] = $r;
		}

		return $result;
	}

	public function getAccessCard($uniqueId) {
		// Generate the pdf.

		$client = $this->getPropertyByUniqueId($uniqueId);
		if(empty($client)) {
			throw new EntityNotFoundException("The requested access card was not found.");
		}

		$pdf = new AttendeeAccessCardPdf($client);
		$pdf->addPage();

		$pdf->printFoldingLines();
		$pdf->printHeader();
		$pdf->printIndicatorColours();
		$pdf->printTtmLogo();
		$pdf->printPartnerLogos();
		$pdf->printAttendeeInfo();
		$pdf->printBlock1();
		$pdf->printBlock2();
		$pdf->printBlock3();
		$pdf->printBlock4();

		// $pdf->printTitle();
		// $pdf->drawCard();

		// Save pdf.

		$scratchDir = config('gold100.scratch_dir');

		$filename = 'attendee_pass_'.$client->id.'.pdf';

		$pdfPath = $scratchDir.DIRECTORY_SEPARATOR.$filename;

		$pdf->Output($pdfPath, 'F');

		return ['path' => $pdfPath];
	}

}