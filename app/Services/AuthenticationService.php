<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

use App\Exceptions\AuthenticationFailedException;
use App\Exceptions\AuthorisationFailedException;
use App\User;

class AuthenticationService
{
	private $request;

	private $authenticatedUser;

	/**
	 * Constructor.
	 */
	public function __construct(
		Request $request)
	{
		$this->request = $request;
	}

	//-----------------------------------------------------------------------------
	// Accessor methods.
	//-----------------------------------------------------------------------------

	public function getAuthenticatedUser()
	{
		if (!empty($this->authenticatedUser))
	    	return $this->authenticatedUser;

	    return $this->request->session()->get('authentcated_user', null);
	}
	
	public function setAuthenticatedUser($authenticatedUser)
	{
	    $this->authenticatedUser = $authenticatedUser;
	    return $this;
	}

	//-----------------------------------------------------------------------------
	// Public methods.
	//-----------------------------------------------------------------------------

	/**
	 * Sets the authenticated user to a specific user.
	 * 
	 * @param integer $id user id
	 * @return void
	 */
	public function setAuthenticatedUserById($id)
	{
		$user = User::find($id);

		if (!empty($user)) {

			$this->setAuthenticatedUser($user);

		} else {

			throw new AuthenticationFailedException('Requesting user not authenticated.');

		}
	}

	/**
	 * Asserts authenticated user is in a role.
	 * 
	 * Throws an authorisation failed exception if user does not have the role.
	 * 
	 * @param string $roleName 
	 * @return void
	 */
	public function assertRole($roleName)
	{
		if (!in_array($roleName, $this->getAuthenticatedUser()->getRoles())) {

			throw new AuthorisationFailedException('User does not have sufficient permission to perform this action.');

		}
	}

	/**
	 * Sets the authenticated user having the provided login credentials.
	 * 
	 * @param string $loginId 
	 * @param string $loginPass 
	 * @return view
	 */
	public function authenticateUser($loginId, $loginPass)
	{
		$user = User::where('login_id', $loginId)->first();

		if (empty($user)) {
			return view('auth.login')->with('error', 'Invalid login credentials(0)');
		}

		if (!Hash::check($loginPass.$user->salt, $user->password)) {
			return view('auth.login')->with('error', 'Invalid login credentials(1)');
		}

		if (!$user->is_active) {
			return view('auth.login')->with('error', 'User has been disabled.');
		}

		$this->request->session()->set('authentcated_user', $user);

		return redirect()->route('home');
	}

	/**
	 * Changes the password of the current logged in user.
	 * 
	 * @param string $currentPassword 
	 * @param string $newPassword 
	 * @param string $repeatNewPassword 
	 * @return view
	 */
	public function changePassword($currentPassword, $newPassword, $repeatNewPassword)
	{
		$authedUser = $this->getAuthenticatedUser();
		
		// Check current password.

		if (!Hash::check($currentPassword.$authedUser->salt,
							$authedUser->password)) {
			return view('auth.change-password', ['error' => 'Current password you entered is incorrect.']);
		}

		// Check new password spec.

		if (strlen($newPassword) < 8) {
			return view('auth.change-password', ['error' => 'New password must be atleast 8 characters.']);
		}

		if (preg_match('/\s/',$newPassword)) {
			return view('auth.change-password', ['error' => 'New password should not contain any whitespaces.']);
		}

		if ($newPassword !== $repeatNewPassword) {
			return view('auth.change-password', ['error' => 'New passwords does not match.']);
		}

		if ($currentPassword === $repeatNewPassword) {
			return view('auth.change-password', ['error' => 'New passwords must not be the same as current password.']);
		}

		// Change password.

		$salt = ''.Uuid::generate(4);

		$hash = Hash::make($newPassword.$salt);

		$authedUser->salt = $salt;
		$authedUser->password = $hash;
		$authedUser->requires_password_change = 0;
		$authedUser->save();

		return view('auth.change-password', ['success' => 'Password changed successfully.']);
	}

	/**
	 * Clears the current authenticated user.
	 * 
	 * @return view
	 */
	public function clearAuthenticatedUser()
	{
		$this->authenticatedUser = null;
		$this->request->session()->forget('authentcated_user');

		return view('auth.login')->with('success', 'You are now logged out.');
	}
}