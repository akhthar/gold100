<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

use App\Notifications\NewAccountCreated;
use App\Notifications\AccountPasswordReset;
use App\User;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;


use App\Exceptions\InvalidOperationException;
use App\Exceptions\ValidationException;
use App\Exceptions\EntityNotFoundException;

class UserService
{
	private $authenticationService;

	/**
	 * Constructor.
	 */
	public function __construct(AuthenticationService $authenticationService)
	{
		$this->authenticationService = $authenticationService;
	}

	//-----------------------------------------------------------------------------
	// Public methods.
	//-----------------------------------------------------------------------------

	public function getUserById($id)
	{
		return User::find($id);
	}

	public function getUserByLoginId($loginId)
	{
		return User::where('username', $loginId)->first();
	}

	public function getUsers($params)
	{
		// Check permission.

		// $this->authenticationService->assertRole('organiser');

		// Extract filters.

		$username = trim(array_get($params, 'username', ''));

		$name = trim(array_get($params, 'name', ''));

		$orderBy = array_get($params, 'order_by', 'name');

		$orderDir = array_get($params, 'order_dir', 'asc');

		$page = array_get($params, 'page', 1);

		$limit = array_get($params, 'limit', 10);

		// Build query.

		$query = User::where('id', '>', 0);

		if (!empty($username)) {
			$query->where('username', 'like', '%'.$username.'%');
		}

		if (!empty($name)) {
			$query->where('name', 'like', '%'.$name.'%');
		}

		$users = $query->orderBy($orderBy, $orderDir)
			->skip(($page - 1) * $limit)
			->take($limit)
			->get();

		// Total record count.

		$query = User::where('id', '>', 0);

		if (!empty($username)) {
			$query->where('username', 'like', '%'.$username.'%');
		}

		if (!empty($name)) {
			$query->where('name', 'like', '%'.$name.'%');
		}

		$count = $query->get()->count();

		// Process results.

		$usersArr = [];

		foreach ($users as $user) {
			$usersArr[] = $user->toArray();
		}

		return [
			'meta' => [
				'pager' => [
					'page' => $page,
					'limit' => $limit,
					'totalCount' => $count,
					'orderBy' => $orderBy,
					'orderDir' => $orderDir
				],
			],
			'data' => $usersArr
		];
	}

	/**
	 * Creates a new user.
	 * 
	 * @return array
	 */
	public function createUser($data)
	{
		// Check permission.

		// $this->authenticationService->assertRole('organiser');

		// Extract parameters.

		$name = trim(array_get($data, 'name', ''));
		
		$username = trim(array_get($data, 'username', ''));

		$email = trim(array_get($data, 'email', ''));

		// Validate.

		if (empty($name)) {
			throw new ValidationException('User name is not valid.');
		}

		if (empty($username) || !ctype_alnum($username)) {
			throw new ValidationException('Login id is not valid.');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('User email is not valid.');
        }

        // Check login id already exists.

        $existingUser = User::where('username', $username)->first();

        if ($existingUser) {
        	throw new ValidationException("Login id already in use.");
        }


        $existingUser = User::where('email', $email)->first();

        if ($existingUser) {
        	throw new ValidationException("Email address already in use.");
        }

        // Create the user.

        // $salt = ''.Uuid::generate(4);
		$password = $this->randomPassword();

        $user = new User();
        $user->name = $name;
        $user->username = $username;
        $user->email = $email;
        // $user->roles = 'organiser';
        // $user->requires_password_change = 1;
        $user->is_active = true;
		// $user->salt = $salt;
		$user->password = Hash::make($password);

		$user->save();

		// Send notification email.

		$user->notify(new NewAccountCreated($username, $password));

        // Done.

        return $user->toArray();

	}

	public function deleteUser($id) {
		$user = User::find($id);

		return $user->delete();
	}

	/**
	 * Resets a user password.
	 * 
	 * @param integer $id 
	 * @return void
	 */
	public function resetPassword($id)
	{
		// Check permission.

		// $this->authenticationService->assertRole('organiser');

		// Retrieve user.

		$user = $this->getUserById($id);

		if (!$user->is_active) {
			throw new InvalidOperationException('Cannot reset password for a disabled user.');
		}

		// Create and save new password and salt.

		// $salt = ''.Uuid::generate(4);
		$password = $this->randomPassword();

		// $user->requires_password_change = 1;
		// $user->salt = $salt;
		$user->password = Hash::make($password);
		$user->save();

		// Send notification email.

		// Mail::to($user)->send(new ResetPassword($user->username, $password));
		$user->notify(new AccountPasswordReset($user->username, $password));

		// Done.

		return $user->toArray();
	}

	public function changePassword($data)
	{
		// Check permission.

		// $this->authenticationService->assertRole('organiser');

		// Retrieve user.

		$user = $this->getUserById(Auth::id());

		if (!$user->is_active) {
			throw new InvalidOperationException('Cannot change password for a disabled user.');
		}

		// Create and save new password and salt.

		$password = trim(array_get($data, 'new_password', 'default'));
		// $salt = ''.Uuid::generate(4);
		$password = $password;

		// $user->requires_password_change = 1;
		// $user->salt = $salt;
		$user->password = Hash::make($password);
		$user->save();

		// Send notification email.

		// Mail::to($user)->send(new ResetPassword($user->username, $password));

		// Done.

		return $user->toArray();
	}

	public function resetForgotPassword($loginId)
	{
		$user = User::where('username', $loginId)->first();

		if (empty($user)) {
			return view('auth.forgot-password', ['error' => 'There was an error resetting your password. Please contact administrator for help (1).']);
		}

		if (!$user->is_active) {
			return view('auth.forgot-password', ['error' => 'There was an error resetting your password. Please contact administrator for help (2).']);
		}

		// Create and save new password and salt.

		$salt = ''.Uuid::generate(4);
		$password = $this->randomPassword();

		$user->requires_password_change = 1;
		$user->salt = $salt;
		$user->password = Hash::make($password.$salt);
		$user->save();

		// Send notification email.

		Mail::to($user)->send(new ResetPassword($user->username, $password));

		// Done.

		return view('auth.forgot-password', ['success' => 'Your password has been reset. Please check your email for instructions on logging back to the connect portal.']);
	}

	/**
	 * Disables a user.
	 * 
	 * @param integer $id 
	 * @return array
	 */
	public function disableUser($id)
	{
		// Check permission.

		$this->authenticationService->assertRole('organiser');

		// Retrieve user.

		$user = $this->getUserById($id);

		// Disable.

		$user->is_active = false;
		$user->save();

		// Done.

		return $user->toArray();
	}

	/**
	 * Activates a disabled user.
	 * 
	 * @param integer $id 
	 * @return array
	 */
	public function activateUser($id)
	{
		// Check permission.

		$this->authenticationService->assertRole('organiser');

		// Retrieve user.

		$user = $this->getUserById($id);

		// Disable.

		$user->is_active = true;
		$user->save();

		// Done.

		return $user->toArray();
	}

	/**
	 * Update user basic info.
	 * 
	 * @param ineteger $id 
	 * @return array
	 */
	public function updateUser($id, $data)
	{
		// Check permission.

		// $this->authenticationService->assertRole('organiser');

		// Retrieve user.

		$user = $this->getUserById($id);

		$name = trim(array_get($data, 'name', ''));
		
		// $username = trim(array_get($data, 'username', ''));

		$email = trim(array_get($data, 'email', ''));

		// Validate.

		if (empty($name)) {
			throw new ValidationException('User name is not valid.');
		}

		// if (empty($username) || !ctype_alnum($username)) {
		// 	throw new ValidationException('Login id is not valid.');
		// }

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('User email is not valid.');
        }

        // Check login id already exists.

        // $existingUser = User::where('username', $username)->first();

        // if ($existingUser) {
        // 	throw new ValidationException("Login id already in use.");
        // }


        $existingUser = User::where('email', $email)->first();

        if ($existingUser->id != $id) {
        	throw new ValidationException("Email address already in use.");
        }

        $user = $this->getUserById($id);

        $user->name = $name;
        $user->email = $email;
		// Save.

		$user->save();

		// Done.

		return $user->toArray();
	}

	/**
	 * Creates and associates a user to a participant.
	 * 
	 * @param Participant $participant
	 * @return array login credentials
	 */
	public function createParticipantUser(Participant $participant)
	{
		$salt = ''.Uuid::generate(4);
		$password = $this->randomPassword();

		$user = new User();
		$user->name = $participant->name;
		$user->username = $participant->email;
		$user->email = $participant->email;
		$user->roles = 'participant;'.$participant->type;
		$user->requires_password_change = true;
		$user->is_active = true;
		$user->salt = $salt;
		$user->password = Hash::make($password.$salt);

		$participant->users()->save($user);

		return [
			'username' => $user->username,
			'password' => $password
		];
	}

	// gets the bubbles count for the current user
	public function getUserBubbles($params) {
		$bubbles = new \StdClass();

		$bubbles->unreadNotificationsCounter = $this->notificationService->getUnreadNotificationsCount();
      	$bubbles->unreadMessagesCounter = $this->messageService->getUnreadMessagesCount();
      	$bubbles->pendingMeetingsCounter = $this->meetingRequestService->getPendingMeetingRequestCount();

      	return $bubbles;
	}


	//-----------------------------------------------------------------------------
	// Helper functions.
	//-----------------------------------------------------------------------------

	/**
	 * Generates a short random string.
	 * 
	 * Source:
	 * http://stackoverflow.com/questions/6101956/generating-a-random-password-in-php
	 */
	private function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array();
	    $alphaLength = strlen($alphabet) - 1;
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass);
	}
}