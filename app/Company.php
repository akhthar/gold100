<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Company extends Model
{
    use SoftDeletes;
    use Notifiable;
	
	protected $dates = ['deleted_at'];

	public function subsidiaries() {
		return $this->hasMany('App\Subsidiary');
	}
}
