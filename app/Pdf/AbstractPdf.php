<?php

namespace App\Pdf;

require_once 'tcpdf_includes.php';

/**
 * Abstract class to create pdfs.
 */
class AbstractPdf extends \TCPDF
{
	public function __construct(
		$pageFormat = PDF_PAGE_FORMAT,
		$pageOrientation = PDF_PAGE_ORIENTATION)
	{
		parent::__construct($pageOrientation, PDF_UNIT, $pageFormat, true, 'UTF-8', false);
	}

	 /**
     * Returns the page width minus left and right margins.
     */
    public function getPageWidthWithoutMargins()
    {
        return $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;
    }

    /**
     * Helper function to reset the document font to defaults.
     */
    public function resetFont()
    {
        $this->setTextColor(0, 0, 0);
        $this->setFillColor(0, 0, 0);
        $this->SetFont('Helvetica', '', 9);
    }

}