<?php

namespace App\Pdf;

class MeetingSchedulePdf extends AbstractPdf
{
	const HEADER_HEIGHT = 15;
    const FOOTER_HEIGHT = 15;

    private $confStartDate = '';
    private $confEndDate = '';

    /**
	 * Constructor.
	 */
	public function __construct($confSettings)
    {
    	parent::__construct('A4', 'P');

    	$this->SetMargins(PDF_MARGIN_LEFT, self::HEADER_HEIGHT, PDF_MARGIN_RIGHT);

        $start = new \DateTime($confSettings->start_date);
        $end = new \DateTime($confSettings->end_date);

        $this->confStartDate = $start->format('d F Y');
        $this->confEndDate = $end->format('d F Y');
    }

    public function printFiltersOrganiserFormat($participant, $day, $state)
    {
        $html = '<table cellpadding="5">
            <tbody>
                <tr>
                    <td style="width: 20%;"><strong>Participant</strong></td><td style="width: 80%;">'.$participant.'</td>
                </tr>
                <tr>
                    <td><strong>Date</strong></td><td>'.$day.'</td>
                </tr>
                <tr>
                    <td><strong>State</strong></td><td>'.$state.'</td>
                </tr>
            </tbody>
        </table>';

        $x = PDF_MARGIN_LEFT;
        $y = $this->getY() + 20;

        $this->writeHTMLCell(
            $this->getPageWidthWithoutMargins(),
            0,
            $x,
            $y,
            $html, 0, 1, 0, true, 'L', true);
    }

    public function printFiltersParticipantFormat($day, $state)
    {
        $html = '<table cellpadding="5">
            <tbody>
                <tr>
                    <td style="width: 20%;"><strong>Date</strong></td><td style="width: 80%;">'.$day.'</td>
                </tr>
                <tr>
                    <td><strong>State</strong></td><td>'.$state.'</td>
                </tr>
            </tbody>
        </table>';

        $x = PDF_MARGIN_LEFT;
        $y = $this->getY() + 20;

        $this->writeHTMLCell(
            $this->getPageWidthWithoutMargins(),
            0,
            $x,
            $y,
            $html, 0, 1, 0, true, 'L', true);
    }

    public function printMeetingsOrganiserFormat($meetings)
    {
        $trhtml = '';

        foreach ($meetings as $meeting) {
            $time = new \DateTime($meeting['meeting_time']);

            $trhtml .= '<tr>
                <td style="width: 30%;">'.$meeting['source_name'].'</td>
                <td style="width: 30%;">'.$meeting['target_name'].'</td>
                <td style="width: 8%;">'.$meeting['stall_number'].'</td>
                <td style="width: 18%;">'.'Day '.$meeting['meeting_day'].' - '.$time->format('H:i').'</td>
                <td style="width: 14%;">'.ucwords($meeting['state']).'</td>
            </tr>';
        }

        $html = '<table border="1" cellpadding="5">
            <thead>
                <tr>
                    <th style="width: 30%;"><strong>Requested By</strong></th>
                    <th style="width: 30%;"><strong>Meeting With</strong></th>
                    <th style="width: 8%;"><strong>Stall</strong></th>
                    <th style="width: 18%;"><strong>Day/Time</strong></th>
                    <th style="width: 14%;"><strong>State</strong></th>
                </tr>
            </thead>
            <tbody>
                '.$trhtml.'
            </tbody>
        </table>';

        $x = PDF_MARGIN_LEFT;
        $y = $this->getY() + 10;

        $this->writeHTMLCell(
            $this->getPageWidthWithoutMargins(),
            0,
            $x,
            $y,
            $html, 0, 1, 0, true, 'L', true);
    }

    public function printMeetingsParticipantFormat($meetings)
    {
        $trhtml = '';

        foreach ($meetings as $meeting) {
            $start = new \DateTime($meeting['meeting_time']);
            $end = new \DateTime($meeting['meeting_end_time']);

            $trhtml .= '<tr>
                <td style="width: 8%;">'.'Day '.$meeting['meeting_day'].'</td>
                <td style="width: 8%;">'.$start->format('H:i').'</td>
                <td style="width: 8%;">'.$end->format('H:i').'</td>
                <td style="width: 27%;">'.$meeting['source_name'].'</td>
                <td style="width: 27%;">'.$meeting['target_name'].'</td>
                <td style="width: 8%;">'.$meeting['stall_number'].'</td>
                <td style="width: 14%;">'.ucwords($meeting['state']).'</td>
            </tr>';
        }

        $html = '<table border="1" cellpadding="5">
            <thead>
                <tr>
                    <th style="width: 8%;"><strong>Day</strong></th>
                    <th style="width: 8%;"><strong>Start</strong></th>
                    <th style="width: 8%;"><strong>End</strong></th>
                    <th style="width: 27%;"><strong>Requested By</strong></th>
                    <th style="width: 27%;"><strong>Meeting With</strong></th>
                    <th style="width: 8%;"><strong>Stall</strong></th>
                    <th style="width: 14%;"><strong>State</strong></th>
                </tr>
            </thead>
            <tbody>
                '.$trhtml.'
            </tbody>
        </table>';

        $x = PDF_MARGIN_LEFT;
        $y = $this->getY() + 10;

        $this->writeHTMLCell(
            $this->getPageWidthWithoutMargins(),
            0,
            $x,
            $y,
            $html, 0, 1, 0, true, 'L', true);
    }

    /**
     * Custom page header.
     * 
     * Overriden from TCPDF class.
     */
    public function Header()
    {
    	$html = '<h3>Travel Trade Maldvies</h3>
    		<span>'.$this->confStartDate.' - '.$this->confEndDate.'</span>';

    	$x = PDF_MARGIN_LEFT;
    	$y = 7;

    	$this->writeHTMLCell(
    		$this->getPageWidthWithoutMargins(),
    		0,
    		$x,
    		$y,
    		$html, 0, 1, 0, true, 'C', true);
    }	

    /**
     * Custom page header.
     * 
     * Overriden from TCPDF class.
     */
    public function Footer()
    {
    	// Nothing.
    }
}