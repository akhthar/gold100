<?php

namespace App\Pdf;

use App\Client;

/**
 * Attendee access card pdf.
 */
class AttendeeAccessCardPdf extends AbstractPdf
{
	const HEADER_HEIGHT = 2;
    const FOOTER_HEIGHT = 2;

    /* Important layout coordinates

        x1   x2   x3
      y1|    |    |
        | B1 | B2 |
      y2|____|____|
        |    |    |
        |    |    |
      y3|____|____|
        |    |    |
      y4| B3 | B4 |
    */

    private $layoutX1 = 16;
    private $layoutX2 = 105;
    private $layoutX3 = 194;

    private $layoutY1 = 5;
    private $layoutY2 = 81;
    private $layoutY3 = 216;
    private $layoutY4 = 292;

    /**
     * Attendee type indicator colours (r, g, b).
     */
    private $attendeeIndicatorColours = [
        'trade-visitor' => [0, 155, 203],
        'media' => [255, 192, 92],
        'visitor' => [93, 197, 222],
        'organizer' => [62, 62, 63],
        'partner' => [88, 198, 222],
        'official' => [128, 130, 133],
        'exhibitor' => [12, 111, 153]
    ];

    /**
     * Selected attendee indicator colour.
     */
    private $attendeeIndicatorColour = [];

    /**
     * Background colour of header.
     */
    private $headerColour = [0, 155, 203];

    /**
     * Headings text colour.
     */
    private $headingsTextColour = [254, 200, 92];

	/**
	 * @var App\Models\Attendee
	 */
	private $attendee;



	/**
	 * Constructor.
	 */
	public function __construct(Client $attendee)
    {
    	parent::__construct('A4', 'P');

        $this->SetAutoPageBreak(false);

    	$this->SetMargins(PDF_MARGIN_LEFT, self::HEADER_HEIGHT, PDF_MARGIN_RIGHT);

    	$this->attendee = $attendee;

        $this->attendeeIndicatorColour = 'exhibitor';

        $this->attendee->type = 'exhibitor';

    }

    /**
     * Prints the access card folding aid lines.
     */
    public function printFoldingLines()
    {
        $style = array('width' => 0.05, 'cap' => 'butt', 'join' => 'miter', 'dash' => 12, 'phase' => 12, 'color' => array(0, 0, 0));

        $this->Line($this->layoutX1, $this->layoutY1, $this->layoutX1, $this->layoutY4, $style);
        $this->Line($this->layoutX2, $this->layoutY1, $this->layoutX2, $this->layoutY4, $style);
        $this->Line($this->layoutX3, $this->layoutY1, $this->layoutX3, $this->layoutY4, $style);

        $this->Line($this->layoutX1, $this->layoutY2, $this->layoutX3, $this->layoutY2, $style);
        $this->Line($this->layoutX1, $this->layoutY3, $this->layoutX3, $this->layoutY3, $style);
    }

    public function printHeader()
    {
        $style = array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => '12', 'phase' => 12, 'color' => array(0, 0, 0));

        $this->Rect(
            $this->layoutX1-0.2,
            $this->layoutY1 + 4, 
            ($this->layoutX3 - $this->layoutX1) + 0.4,
            16.5,
            'DF',
            $style,
            $this->headerColour);

        $text = 'THIS IS YOUR ACCESS CARD FOR GUESTHOUSES MALDIVES CONFERENCE';

        $html = '<span style="font-weight: bold; font-size: 12pt; color: #fff;">'.$text.'</span>';

        $this->writeHTMLCell(
            150,
            10,
            $this->layoutX1 + 15,
            $this->layoutY1 + 10,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'C',    // align,
            true    // autopadding
        );
    }

    /**
     * Prints the attendee type indicator colours.
     */
    public function printIndicatorColours()
    {
        $style = array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => '12', 'phase' => 12, 'color' => array(0, 0, 0));

        $this->Rect(
            $this->layoutX1,
            $this->layoutY2, 
            $this->layoutX2 - $this->layoutX1,
            ($this->layoutY2 - $this->layoutY1),
            'DF',
            $style,
            $this->attendeeIndicatorColour);

        $this->Rect(
            $this->layoutX1,
            $this->layoutY2 + 112.5, 
            52.5,
            11.5,
            'DF',
            $style,
            $this->attendeeIndicatorColour);

        $this->Rect(
            $this->layoutX2,
            $this->layoutY3 - 17, 
            $this->layoutX3 - $this->layoutX2,
            17,
            'DF',
            $style,
            $this->attendeeIndicatorColour);
    }

    /**
     * Prints the attendee info.
     */
    public function printAttendeeInfo()
    {
        // $fontname = \TCPDF_FONTS::addTTFfont('C:\Users\Ashfag\Downloads\gothamhtf\arial.ttf');
        // var_dump($fontname); exit();

        // gothamhtf book: gothamhtfbook
        // gothamhtf bold: gothamhtfb
        // gothamhtf medium: gothamhtfmedium
        // gotham black: gothamblack
        // arial

        $name = $this->attendee->property_name;
        // $participantName = $this->attendee->type === 'participant'
        //     ? $this->attendee->participant->name
        //     : '';

        // set participant name as Madives Getaways for organisers and officials
        // TODO: add another field in conference settings for organiser's name. so that we would not need to 
        // hard-code the organiser's name here
        // $participantName = '';
        // if ($this->attendee->type === 'exhibitor') {
        //     $participantName = $this->attendee->property_name;
        // } else if ($this->attendee->type === 'organizer' || $this->attendee->type === 'official') {
        //     $participantName = 'Maldives Getaways';
        // }

        // $country = $this->attendee->type === 'participant'
        //     ? $this->attendee->participant->country
        //     : '';

        $island = $this->attendee->island->atoll->short_name . '. ' . $this->attendee->island->name_en;

        // Print attendee name.

        $html = '<p><span style="font-family: gothamhtfb;; font-size: 10pt;">'.$name.'</span><br /><br /><span style="font-family: gothamhtfmedium; font-size: 9pt;">'.$island.'</span></p>';

        $this->writeHTMLCell(
            50,
            10,
            $this->layoutX1 + 10,
            $this->layoutY2 + 87,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );

        // Print country.

        // $html = '<p style="font-family: gothamhtfmedium; font-size: 9pt;">'.$country.'</p>';

        // $this->writeHTMLCell(
        //     50,
        //     10,
        //     $this->layoutX1 + 10,
        //     $this->layoutY2 + 105,
        //     $html,
        //     0,      // border
        //     0,      // ln
        //     0,      // fill
        //     true,   // reseth
        //     'L',    // align,
        //     true    // autopadding
        // );

        // Print barcode.

        // $aid = $this->attendee->aid;

        $style = array(
            'position' => '',
            'align' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 1
        );

        $this->StartTransform();
        $this->Rotate(90, $this->layoutX1 + 65.5, $this->layoutY2 + 127);

        // $this->write1DBarcode(
        //     $aid,
        //     'C128A',
        //     $this->layoutX1 + 65.5,
        //     $this->layoutY2 + 127,
        //     70,
        //     13,
        //     0.3,
        //     $style,
        //     'N'
        // );

        $this->StopTransform();

        // Print attendee type.

        $this->setFont('gothamblack');

        $attendeeType =  $this->attendee->type === 'participant'
            ? $this->attendee->participant->type
            : $this->attendee->type;

        $html = '<span style="font-size: 13pt; text-transform: uppercase; color: #fff;">'.$attendeeType.'</span>';
         
        $this->writeHTMLCell(
            50,
            10,
            $this->layoutX1 + 10,
            $this->layoutY2 + 115.5,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );

        $html = '<span style="font-size: 16pt; text-transform: uppercase; color: #fff;">'.$attendeeType.'</span>';
         
        $this->writeHTMLCell(
            $this->layoutX3 - $this->layoutX2,
            17,
            $this->layoutX2,
            $this->layoutY3 - 12,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'C',    // align,
            true    // autopadding
        );
    }

    /**
     * Prints partner logos.
     */
    public function printPartnerLogos()
    {
        $this->setFont('gothamhtfmedium', '', '5.5pt');

        $headingWidth = 72.1;
        $lineLeftX = $this->layoutX2 + 8.5;
        $lineRightX = $this->layoutX3 - 8.5;

        $style = array('width' => 0.05, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'phase' => 12, 'color' => array(0, 0, 0));
        
        // Platinum partner and official card.

        $textWidth = 44;
        $y  = 8.75;

        $this->Line(
            $lineLeftX,
            $this->layoutY2 + $y,
            $lineLeftX + ($lineRightX - $lineLeftX- $textWidth) / 2,
            $this->layoutY2 + $y,
            $style
        );

        $this->Line(
            $lineRightX - (($lineRightX - $lineLeftX- $textWidth) / 2),
            $this->layoutY2 + $y,
            $lineRightX,
            $this->layoutY2 + $y,
            $style
        );

        $text = 'PLATINUM PARTNER & OFFICIAL CARD';
        $this->MultiCell(
            $headingWidth,
            5,
            $text,
            0,      // border
            'C',    // Align
            false,   // Fill
            1,      // ln
            $lineLeftX,   // x
            $this->layoutY2 + ($y - 1.5),   // y
            true,   // reseth
            0,      // stretch
            false,  // ishtml
            true,   // autopadding
            0,      // maxh
            'C',    // vertical align
            false   // fitcell
        );

        // Gold partner.

        $textWidth = 22;
        $y  = 35.5; //39.25

        $this->Line(
            $lineLeftX,
            $this->layoutY2 + $y,
            $lineLeftX + ($lineRightX - $lineLeftX- $textWidth) / 2,
            $this->layoutY2 + $y,
            $style
        );

        $this->Line(
            $lineRightX - (($lineRightX - $lineLeftX- $textWidth) / 2),
            $this->layoutY2 + $y,
            $lineRightX,
            $this->layoutY2 + $y,
            $style
        );

        $text = 'GOLD PARTNER';
        $this->MultiCell(
            $headingWidth,
            5,
            $text,
            0,      // border
            'C',    // Align
            false,   // Fill
            1,      // ln
            $lineLeftX,   // x
            $this->layoutY2 + ($y - 1.5),   // y
            true,   // reseth
            0,      // stretch
            false,  // ishtml
            true,   // autopadding
            0,      // maxh
            'C',    // vertical align
            false   // fitcell
        );

        // Silver partner.

        $textWidth = 24;
        $y  = 58.75; // 62.5

        $this->Line(
            $lineLeftX,
            $this->layoutY2 + $y,
            $lineLeftX + ($lineRightX - $lineLeftX- $textWidth) / 2,
            $this->layoutY2 + $y,
            $style
        );

        $this->Line(
            $lineRightX - (($lineRightX - $lineLeftX- $textWidth) / 2),
            $this->layoutY2 + $y,
            $lineRightX,
            $this->layoutY2 + $y,
            $style
        );

        $text = 'SILVER PARTNERS';
        $this->MultiCell(
            $headingWidth,
            5,
            $text,
            0,      // border
            'C',    // Align
            false,   // Fill
            1,      // ln
            $lineLeftX,   // x
            $this->layoutY2 + ($y - 1.5),   // y
            true,   // reseth
            0,      // stretch
            false,  // ishtml
            true,   // autopadding
            0,      // maxh
            'C',    // vertical align
            false   // fitcell
        );

        // Premium exhibitors.

        $textWidth = 27;
        $y  = 96.65; //92.9

        $this->Line(
            $lineLeftX,
            $this->layoutY2 + $y,
            $lineLeftX + ($lineRightX - $lineLeftX- $textWidth) / 2,
            $this->layoutY2 + $y,
            $style
        );

        $this->Line(
            $lineRightX - (($lineRightX - $lineLeftX- $textWidth) / 2),
            $this->layoutY2 + $y,
            $lineRightX,
            $this->layoutY2 + $y,
            $style
        );

        $text = 'PREMIUM EXHIBITORS';
        $this->MultiCell(
            $headingWidth,
            5,
            $text,
            0,      // border
            'C',    // Align
            false,   // Fill
            1,      // ln
            $lineLeftX,   // x
            $this->layoutY2 + ($y - 1.5),   // y
            true,   // reseth
            0,      // stretch
            false,  // ishtml
            true,   // autopadding
            0,      // maxh
            'C',    // vertical align
            false   // fitcell
        );

        // Print all partner logos.

        $this->Image(
            K_PATH_IMAGES.'bml.jpg',
            $this->layoutX2 + 20.75,
            $this->layoutY2 + 14,
            13
        );

        $this->Image(
            K_PATH_IMAGES.'americanexpress.png', // American Express
            $this->layoutX2 + 58,
            $this->layoutY2 + 14,
            15
        );

        $this->Image(
            K_PATH_IMAGES.'mtcc.jpg',
            $this->layoutX2 + 20,
            $this->layoutY2 + 42.25,
            10.25
        );

        $this->Image(
            K_PATH_IMAGES.'dhiraagu.jpg',
            $this->layoutX2 + 39,
            $this->layoutY2 + 40.75,
            10.25
        );

        $this->Image(
            K_PATH_IMAGES.'sto.jpg',
            $this->layoutX2 + 58,
            $this->layoutY2 + 43.25,
            12
        );

        $this->Image(
            K_PATH_IMAGES.'allied.jpg',
            $this->layoutX2 + 15,
            $this->layoutY2 + 65,
            0,
            6
        );

        $this->Image(
            K_PATH_IMAGES.'m7print.jpg',
            $this->layoutX2 + 27,
            $this->layoutY2 + 66,
            0,
            4
        );

        $this->Image(
            K_PATH_IMAGES.'cp.png',
            $this->layoutX2 + 49,
            $this->layoutY2 + 65,
            0,
            4.5
        );

        $this->Image(
            K_PATH_IMAGES.'maldivesairports.jpg',
            $this->layoutX2 + 60,
            $this->layoutY2 + 64,
            0,
            6
        );

        $this->Image(
            K_PATH_IMAGES.'mwsc.jpg',
            $this->layoutX2 + 15,
            $this->layoutY2 + 75,
            0,
            4.5
        );

        $this->Image(
            K_PATH_IMAGES.'asmarine.jpg',
            $this->layoutX2 + 30,
            $this->layoutY2 + 75,
            0,
            3
        );

        $this->Image(
            K_PATH_IMAGES.'maldivegas.jpg',
            $this->layoutX2 + 52,
            $this->layoutY2 + 73,
            0,
            7
        );

        $this->Image(
            K_PATH_IMAGES.'lagoon.png',
            $this->layoutX2 + 62,
            $this->layoutY2 + 68,
            0,
            15
        );

        $this->Image(
            K_PATH_IMAGES.'mohanmutha.jpg',
            $this->layoutX2 + 21,
            $this->layoutY2 + 84,
            0,
            5
        );

        $this->Image(
            K_PATH_IMAGES.'onestop.jpg',
            $this->layoutX2 + 42,
            $this->layoutY2 + 81,
            0,
            9
        );

        $this->Image(
            K_PATH_IMAGES.'eventmaldives.jpg',
            $this->layoutX2 + 56,
            $this->layoutY2 + 83,
            0,
            5
        );

        $this->Image(
            K_PATH_IMAGES.'adaaran.jpg',
            $this->layoutX2 + 11,
            $this->layoutY2 + 100,
            0,
            10
        );

        $this->Image(
            K_PATH_IMAGES.'bandos.jpg',
            $this->layoutX2 + 22,
            $this->layoutY2 + 98,
            0,
            15
        );

        $this->Image(
            K_PATH_IMAGES.'crownchampa.jpg',
            $this->layoutX2 + 34.5,
            $this->layoutY2 + 101.5,
            0,
            8.5
        );

        $this->Image(
            K_PATH_IMAGES.'universalresorts.jpg',
            $this->layoutX2 + 51,
            $this->layoutY2 + 103,
            0,
            3.5
        );

        $this->Image(
            K_PATH_IMAGES.'villahotels.jpg',
            $this->layoutX2 + 66,
            $this->layoutY2 + 102,
            0,
            4.5
        );
    }

    /**
     * Prints all the images that apppear on the pdf.
     */
    public function printTtmLogo()
    {
        $this->Image(
            K_PATH_IMAGES.'TTM-LOGO.png',
            $this->layoutX1 + 52.5,
            $this->layoutY2 + 14.2,
            23.8
        );
    }

    /**
     * Prints block 1 texts.
     */
    public function printBlock1()
    {
        $this->printTextHeading(
            $this->layoutX1 + 6.75,
            $this->layoutY1 + 25,
            'IMPORTANT');

        $this->SetTextColor(0, 0, 0);
        $this->setFont('arial', '', '6.5pt');

        $html = '<p>This badge is valid from 12- 13th July 2017.</p>';
        $this->writeHTMLCell(
            80,
            10,
            $this->layoutX1 + 7,
            $this->layoutY1 + 33,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );

        $this->printTextHeading(
            $this->layoutX1 + 6.75,
            $this->layoutY1 + 45.5,
            'YOUR ENTRANCE');

        $this->SetTextColor(0, 0, 0);
        $this->setFont('arial', '', '6.5pt');

        $html = '<p>The entrance to TTM is from The National Art Gallery main entrance in Medhuziyaarai Magu.</p>';
        $this->writeHTMLCell(
            69,
            10,
            $this->layoutX1 + 7,
            $this->layoutY1 + 54,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );
    }

    /**
     * Prints block 2 texts.
     */
    public function printBlock2()
    {
        $this->printTextHeading(
            $this->layoutX2 + 4.5,
            $this->layoutY1 + 25,
            'WHAT IF YOU FORGET THE BADGE?');

        $this->SetTextColor(0, 0, 0);
        $this->setFont('arial', '', '6.5pt');

        $html = '<p>Please note that you will need to get another e-badge printed at reception to gain entry to TTM. We strongly advise that you print your badge before coming to the event to avoid delays.</p><p>By using the TTM e-badge you agree to observe the event admission policy. For more information, visit www.traveltrademaldives.com</p><p>TTM  e-badge  are  strictly  non-transferable  and  only  valid  for  the  person  whose  name  is  printed  on  it,  on  the  days  shown.  You  may  be  asked  to  validate your e-badge with a personal photographic identification document  (ID or passport). E-badge must be protected from liquids and must not be folded  across  the  e-badge  barcodes.  Damaged  e-badges  may  delay  your  entry  into  the  event.  TTM  reserves  right  to  reject  e-badges  and  refuse  admission into the event should tickets be copies or fraudulently produced.</p>';
        $this->writeHTMLCell(
            80,
            10,
            $this->layoutX2 + 4.5,
            $this->layoutY1 + 33,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );
    }

    /**
     * Prints block 3 texts.
     */
    public function printBlock3()
    {
        $this->printTextHeading(
            $this->layoutX1 + 6.75,
            $this->layoutY3 + 7.5,
            'OPENING TIMES AND ADMISSION POLICY');

        $this->SetTextColor(0, 0, 0);
        $this->setFont('arial', '', '6.5pt');

        $html = '<p>Wednesday, 12th July 2017 --------------------------------- 11:00 am - 06:00 pm<br />
        Thursday, 13th July 2017 ------------------------------------ 09:00 am - 06:00 pm</p>
        <br />
        <br />

        Admission to registered Exhibitors, Visitors, Media, Speakers, Officals.<br />TTM is a travel trade event and, therefore, all visitors must be working in or be associated to, the travel industry. <strong style="font-family: gothamhtfb">Attendance is strictly limited to persons over the age of 16 years.</strong>';

        $this->writeHTMLCell(
            80,
            10,
            $this->layoutX1 + 6.75,
            $this->layoutY3 + 16,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );
    }

    /**
     * Prints block 4 texts.
     */
    public function printBlock4()
    {
        $this->printTextHeading(
            $this->layoutX2 + 10,
            $this->layoutY3 + 7.5,
            'PRINTING & FOLDING INSTRUCTION');

        $this->SetTextColor(0, 0, 0);
        $this->setFont('arial', '', '6.5pt');

        // $html = '<ol>
        //     <li>Check your printer settings are set to 100% and print on A4 paper in colour</li>
        //     <li>Fold along the dotted lines as presented below</li>
        //     <li>Present your pre-printed badge upon arrival at the CTICC and collect your badge holder and lanyard at your entrance</li>
        // </ol>';

        $html = '<p><span style="color: #FEC85C; font-family: gothamhtfb;">1.</span> Check your printer settings are set to 100% and print on<br />&nbsp;&nbsp;&nbsp;&nbsp;A4 paper in colour
            <br /><br />
            <span style="color: #FEC85C; font-family: gothamhtfb;">2.</span> Fold along the dotted lines as presented below
            <br /><br />
            <span style="color: #FEC85C; font-family: gothamhtfb;">3.</span> Present your pre-printed badge upon arrival at the organiser\'s desk<br />&nbsp;&nbsp;&nbsp;&nbsp;and collect your badge holder and lanyard at your entrance
        </p>';

        $this->writeHTMLCell(
            80,
            10,
            $this->layoutX2 + 10,
            $this->layoutY3 + 16,
            $html,
            0,      // border
            0,      // ln
            0,      // fill
            true,   // reseth
            'L',    // align,
            true    // autopadding
        );

        $this->Image(
            K_PATH_IMAGES.'folding.jpg',
            $this->layoutX2 + 13,
            $this->layoutY3 + 40,
            70
        );
    }

    /**
     * Helper function to print a heading.
     */
    private function printTextHeading($x, $y, $text)
    {
        $this->SetTextColor($this->headingsTextColour[0], $this->headingsTextColour[1], $this->headingsTextColour[2]);
        $this->setFont('gothamhtfb', '', '10pt');

        $this->MultiCell(
            80,
            5,
            $text,
            0,      // border
            'L',    // Align
            false,   // Fill
            1,      // ln
            $x,   // x
            $y,   // y
            true,   // reseth
            0,      // stretch
            false,  // ishtml
            true,   // autopadding
            0,      // maxh
            'C',    // vertical align
            false   // fitcell
        );

        $style = array('width' => 0.45, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'phase' => 12, 'color' => $this->headingsTextColour);
        
        $this->Line(
            $x + 1,
            $y + 6,
            $x + 2.75,
            $y + 6,
            $style
        );
    }

    /**
     * Custom page header.
     * 
     * Overriden from TCPDF class.
     */
    public function Header()
    {
    	// Nothing.
    }

    /**
     * Custom page header.
     * 
     * Overriden from TCPDF class.
     */
    public function Footer()
    {
    	// Nothing.
    }
}