<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Atoll;
// use App\Island;
use App\Company;
use App\Services\PropertyService;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    private $propertyService;
    /**
     * Constructor.
     */
    function __construct(PropertyService $propertyService)
    {
        // $this->messageService = $messageService;
        // parent::__construct();
        $this->propertyService = $propertyService;
    }

    public function index() {
        // echo Hash::make('welcome');
        // die();
       // $data['atolls'] = Atoll::orderBy('id')->get();
        $data = [];
        return view('register')->with($data);
    }

    public function fetchIslands($atollId) {
        $islands = Island::where('atoll_id', $atollId)->orderBy('name_en')->get();
        
        //$json = $islands->toArray();

        return new \Illuminate\Http\JsonResponse($islands->toArray(), 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function register(Request $request) {
        $data = json_decode($request->getContent(), true);

        $client = $this->propertyService->createClient($data, $request);
        
        return new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);
    }

    public function uploadCompanyLogo(Request $request) {
        $file = $request->file('qqfile');

        $json = $this->propertyService->addCompanyLogo($request, $file, true);

        return new \Illuminate\Http\JsonResponse($json, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function uploadCompanyRegistration(Request $request) {
        $file = $request->file('qqfile');

        $json = $this->propertyService->addCompanyRegistration($request, $file, true);

        return new \Illuminate\Http\JsonResponse($json, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function deleteCompanyLogo($id, Request $request) {
        
        $json = $this->propertyService->deleteCompanyLogo($id);
                
      //  return true;
    }

    public function deleteCompanyRegistration($id, Request $request) {
        
        $json = $this->propertyService->deleteCompanyRegistration($id);
                
      //  return true;
    }

    public function downloadLogo($id, Request $request) {
        $logo = $this->propertyService->getMagazinePhoto($id);

        $path = public_path().DIRECTORY_SEPARATOR.'profile_photos'.DIRECTORY_SEPARATOR.$logo->filename;

        return response()->download($path);
    }


    public function downloadConferencePass($uniqueId) {
        $json = $this->propertyService->getAccessCard($uniqueId);

        $filepath = $json['path'];
        $filename = 'conference_pass_'.$this->randomString().'.pdf';

        $headers = [
            'Content-Type: application/pdf',
        ];

        return response()->download($filepath, $filename, $headers);
    }


    //-----------------------------------------------------------------------------
    // Helper functions.
    //-----------------------------------------------------------------------------

    /**
     * Generates a short random string.
     * 
     * Source:
     * http://stackoverflow.com/questions/6101956/generating-a-random-password-in-php
     */
    private function randomString() {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
