<?php

namespace App\Http\Controllers\cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\UserService;

use App\Client;

class UserController extends Controller
{
    private $UserService;

    public function __construct(UserService $UserService) {
        $this->middleware('auth');
        $this->UserService = $UserService;
    }

    public function index() {
        $data = [];
        $data['selected_menu'] = 'users';
        $data['parent_menu'] = 'users';

        return view('dashboard.users')->with($data);
    }

    public function getUsers(Request $request) {
    	$data = json_decode($request->getContent(), true);

        $users = $this->UserService->getUsers($data);
        
        return new \Illuminate\Http\JsonResponse($users, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function createUser(Request $request) {
        $data = json_decode($request->getContent(), true);

        $user = $this->UserService->createUser($data);
        
        return new \Illuminate\Http\JsonResponse($user, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function resetPassword($id, Request $request) {
        

        $user = $this->UserService->resetPassword($id);
        
        return new \Illuminate\Http\JsonResponse($user, 201, [], JSON_UNESCAPED_SLASHES);
    }

    public function updateUser($id, Request $request) {
        $data = json_decode($request->getContent(), true);

        $user = $this->UserService->updateUser($id, $data);
        
        return new \Illuminate\Http\JsonResponse($user, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function deleteUser($id, Request $request) {
        $this->UserService->deleteUser($id);

        // return new \Illuminate\Http\JsonResponse([], 204, [], JSON_UNESCAPED_SLASHES);
        return;
    }

    public function changePassword(Request $request) {
        $data = json_decode($request->getContent(), true);

        $user = $this->UserService->changePassword($data);
        
        return new \Illuminate\Http\JsonResponse($user, 201, [], JSON_UNESCAPED_SLASHES);
    }
}
