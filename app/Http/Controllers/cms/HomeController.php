<?php

namespace App\Http\Controllers\cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\PropertyService;
use League\Csv\Writer;

use App\Company;
use App\User;

class HomeController extends Controller
{
    private $propertyService;

    public function __construct(PropertyService $propertyService) {
        $this->middleware('auth');
        $this->propertyService = $propertyService;
    }

    public function index() {
        $data = [];
        $data['selected_menu'] = 'dashboard';
        $data['parent_menu'] = 'dashboard';


        // fetch some data for dashboard
        $newRequestsCount = Company::where('is_registered', false)->where('is_rejected', false)->where('deleted_at', null)->get()->count();
        $registeredClientsCount = Company::where('is_registered', true)->where('is_rejected', false)->where('deleted_at', null)->get()->count();
        $usersCount = User::where('is_active', true)->get()->count();

        $data['newRequestsCount'] = $newRequestsCount;
        $data['registeredClientsCount'] = $registeredClientsCount;
        $data['usersCount'] = $usersCount;

        return view('dashboard.index')->with($data);
    }

    public function getRequests(Request $request) {

    	$data = [];
    	$data['selected_menu'] = 'new-requests';
        $data['parent_menu'] = 'properties';

        return view('dashboard.new-requests')->with($data);
    }

    public function getNewRequests(Request $request) {
        $data = json_decode($request->getContent(), true);

        $clients = $this->propertyService->getNewCompanies($data);
        
        return new \Illuminate\Http\JsonResponse($clients, 200, [], JSON_UNESCAPED_SLASHES);
    }

     public function getRejected(Request $request) {

        $data = [];
        $data['selected_menu'] = 'rejected';
        $data['parent_menu'] = 'properties';

        return view('dashboard.rejected-companies')->with($data);
    }

    public function getRejectedCompanies(Request $request) {
        $data = json_decode($request->getContent(), true);

        $clients = $this->propertyService->getRejectedCompanies($data);
        
        return new \Illuminate\Http\JsonResponse($clients, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function acceptRequest($id, Request $request) {
        

        $client = $this->propertyService->acceptRegistrationRequest($id, $request);

        return new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);

    }

    public function rejectRequest($id, Request $request) {
        

        $client = $this->propertyService->rejectRegistrationRequest($id, $request);

        return new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);

    }

    public function deleteRequest($id, Request $request) {
        $client = Company::find($id);
        $client->is_registered = false;
        $client->save();
        $client->delete();
        // $client->save();

        return;// new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);

    }

    public function revokeRequest($id, Request $request) {
        $client = Company::find($id);

        $client->is_registered = false;
        $client->save();

        return new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);

    }

    public function registeredCompanies(Request $request) {
        $data = [];
        $data['selected_menu'] = 'registered';
        $data['parent_menu'] = 'properties';

        return view('dashboard.registered-properties')->with($data);
    }

    public function getRegisteredCompanies(Request $request) {
        $data = json_decode($request->getContent(), true);
        $params = $request->all();

        $clients = $this->propertyService->getRegisteredCompanies($params);
        
        return new \Illuminate\Http\JsonResponse($clients, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function downloadComapniesCSV(Request $request) {
        $companies = Company::with(['subsidiaries'])->where('is_registered', true)->where('is_rejected', false)->orderBY('company_name', 'asc')->get();

        $fileName = 'gold100' . '_' . time();

        $companiesArray = [];

        foreach($companies as $k => $c) {
            $companiesArray[] = $c;
        }

        // echo '<pre>';
        // print_r($companiesArray);
        // die();


        \Excel::create($fileName, function($excel) use($companies) {

            $excel->sheet('Registred Companies', function($sheet) use($companies) {

                //$sheet->fromArray($pArray);
                // foreach($companiesArray) {
                    // $sheet->fromArray($companiesArray);
                // }
                $row = 1;
                $sheet->row($row, array(
                    'company_name',
                    'registration_number',
                    'managing_director',
                    'registration_date',
                    'number_of_employees',
                    'revenue_currency_2022',
                    'annual_gross_revenue_2022',
                    'revenue_currency_2021',
                    'annual_gross_revenue_2021',
                    'revenue_currency_2020',
                    'annual_gross_revenue_2020',
                    'industry',
                    'introduction',
                    'products_and_services',
                    'business_success_and_innovativeness',
                    'address',
                    'contact_number',
                    'email',
                    'website',
                    'contact_person',
                    'contact_person_number',
                    'contact_email',
                    'subsidiaries'
                ));
                


                    foreach($companies as $key => $co) {
                        $subsidiaries = '';
                        foreach($co->subsidiaries as $sk => $s) {
                            $subsidiaries .= $s->name . ' | ';
                        }

                        $row++;
                        $sheet->row($row, array(
                            $co->company_name,
                            $co->registration_number,
                            $co->managing_director,
                            $co->registration_date,
                            $co->number_of_employees,
                            $co->revenue_currency_2017,
                            $co->annual_gross_revenue_2017,
                            $co->revenue_currency_2016,
                            $co->annual_gross_revenue_2016,
                            $co->revenue_currency_2015,
                            $co->annual_gross_revenue_2015,
                            $co->industry,
                            $co->introduction,
                            $co->products_and_services,
                            $co->business_success_and_innovativeness,
                            $co->address,
                            $co->contact_number,
                            $co->email,
                            $co->website,
                            $co->contact_person,
                            $co->contact_person_number,
                            $co->contact_email,
                            $subsidiaries
                        ));
                    }
            });

           

            // $excel->sheet('Property Policies', function($sheet) use($property) {

            //     $sheet->fromArray($property->policies);
            //    // $sheet->fromArray($pArray['policies'], null, 'A1', true, false);
            //    // $sheet->rows(array($policies));

            // });

            

        })->export('xls');
    }

//     public function downloadCSV() {
//         $data = $this->propertyService->downloadCSV();

//         $filename = 'gold100_registered_companies_list_'. time() .'.csv';

//             $csvData = $this->formatToCsv(
//                 $data,
//                 ['company_name','ceo_managing_director','year_started','employment','revenue','industry','ownership','subsidiaries','introduction','products_and_services','business_success_and_innovativeness','address',
// 'contact_number','email','website'],
//                 ['company_name','ceo_managing_director','year_started','employment','revenue','industry','ownership','subsidiaries','introduction','products_and_services','business_success_and_innovativeness','address',
// 'contact_number','email','website']
//             );

//             return response($csvData)
//                 ->header('Content-Type', 'text/csv; charset=UTF-8')
//                 ->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
//     }

//     private function formatToCsv($data, $fieldTitles, $fieldNames)
//     {
//         if (!ini_get("auto_detect_line_endings")) {
//             ini_set("auto_detect_line_endings", '1');
//         }

//         $csv = Writer::createFromFileObject(new \SplTempFileObject());
//         $csv->insertOne($fieldTitles);

//         foreach ($data as $d) {
//             // echo '<pre>';
//             // print_r($d);
//             // die();
//             $row = [];
//             foreach ($fieldNames as $fieldName) {
//                 $row[] = $d->$fieldName;
//             }

//             $csv->insertOne($row);
//         }

//         return $csv->__toString();
//     }

//     public function sendCardsLink() {

//     }

    public function getCompany($id, Request $request) {
        $data = [];
        $data['selected_menu'] = 'rejected';
        $data['parent_menu'] = 'properties';
        $data['companyId'] = $id;

        return view('dashboard.edit-company')->with($data);
    }

    public function getCompanyInfo($id, Request $request) {
        $company = Company::with(['subsidiaries'])->find($id);

        return new \Illuminate\Http\JsonResponse($company, 200, [], JSON_UNESCAPED_SLASHES);
    }


    public function update($id, Request $request) {
        $data = json_decode($request->getContent(), true);

        $client = $this->propertyService->updateClient($data, $request);
        
        return new \Illuminate\Http\JsonResponse($client->toArray(), 201, [], JSON_UNESCAPED_SLASHES);
    }

}
