<?php

namespace App\Exceptions;

/**
 * Root exception class for exceptions thrown from Connect Point API.
 */
abstract class ConnectPointException extends \Exception
{
	public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}