<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ConnectPointException) {

            return $this->handleConnectPointException($request, $exception);

        } else if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

            $body = [
                'errors' => [
                    ['code' => 'token-mismatch', 'title' => 'Token Mismatch', 'detail' => 'Your form has expired. Please refresh your browser and try again.']
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 403, [], JSON_UNESCAPED_SLASHES);
        } else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->view('notfound', [], 404);
        } else {

            return parent::render($request, $exception);

        }
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }

    private function handleConnectPointException($request, $exception)
    {
        if ($exception instanceof AuthenticationFailedException) {

            $body = [
                'errors' => [
                    ['code' => 'authentication-failed', 'title' => 'Authentication Failed', 'detail' => $exception->getMessage()]
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 401, [], JSON_UNESCAPED_SLASHES);

        } else if ($exception instanceof AuthorisationFailedException) {

            $body = [
                'errors' => [
                    ['code' => 'authorisation-failed', 'title' => 'Authorisation Failed', 'detail' => $exception->getMessage()]
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 403, [], JSON_UNESCAPED_SLASHES);

        } else if ($exception instanceof InvalidOperationException) {

            $body = [
                'errors' => [
                    ['code' => 'invalid-operation', 'title' => 'Invalid Operation', 'detail' => $exception->getMessage()]
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 403, [], JSON_UNESCAPED_SLASHES);

        } else if ($exception instanceof ValidationException) {

            $body = [
                'errors' => [
                    ['code' => 'validation-failure', 'title' => 'Validation Failure', 'detail' => $exception->getMessage()]
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 403, [], JSON_UNESCAPED_SLASHES);

        } else if ($exception instanceof EntityNotFoundException) {

            $body = [
                'errors' => [
                    ['code' => 'entity-not-found', 'title' => 'Entity Not Found', 'detail' => $exception->getMessage()]
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 404, [], JSON_UNESCAPED_SLASHES);

        } else if ($exception instanceof TokenMismatchException) {

            $body = [
                'errors' => [
                    ['code' => 'token-mismatch', 'title' => 'Token Mismatch', 'detail' => 'Your form has expired. Please refresh your browser and try again.']
                ]
            ];

            return new \Illuminate\Http\JsonResponse($body, 403, [], JSON_UNESCAPED_SLASHES);

        } else {

            return parent::render($request, $exception);

        }
    }
}
