<?php

namespace App\Exceptions;

class AuthenticationFailedException extends ConnectPointException
{
	public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
    }
}