<?php

namespace App\Exceptions;

class AuthorisationFailedException extends ConnectPointException
{
    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
    }
}