<?php

namespace App\Exceptions;

class ValidationException extends ConnectPointException
{
    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
    }
}
