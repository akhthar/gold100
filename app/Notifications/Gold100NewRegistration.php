<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Company;
use \DateTime;

class Gold100NewRegistration extends Notification
{
    use Queueable;

    public $company;
    public $recipientName;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, $recipientName)
    {
        $this->company= $company;
        $this->recipientName = $recipientName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('admin/requests');
        return (new MailMessage)
                    ->subject('Gold100 New Information Form Submission')
                    ->greeting('Hello')
                    ->line('New company information form has been submitted to Gold100.')
                    ->line('Company: ' . $this->company->company_name)
                    ->line('Time: ' . (new DateTime($this->company->created_at))->format('d-m-Y H:i:s'))
                    ->line('IP Address: ' . $this->company->client_ip_address)
                  //  ->line('Requested By: ', $this->leave->requestedBy->name)
                  //  ->line('Requested On: ', (new DateTime($this->leave->created_at))->format('d-m-Y H:i:s'))
                    ->action('USE THIS LINK TO TAKE ACTION', $url)
                    ->salutation('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
