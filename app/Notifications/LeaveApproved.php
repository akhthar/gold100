<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\Leave;
use \DateTime;

class LeaveApproved extends Notification
{
    use Queueable;

    public $leave;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Leave $leave)
    {
        $this->leave = $leave;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello ' . $this->leave->employee->name)
                    ->line('Your leave request has been approved.')
                    
                    ->line('Leave Type: ' . $this->leave->leaveType->leave_name)
                    ->line('Leave Start Date: ' . (new DateTime($this->leave->start_date))->format('d-m-Y'))
                    ->line('Expected End Date: ' . (new DateTime($this->leave->expected_end_date))->format('d-m-Y'))
                    ->line('Number of Days Requested: ' . $this->leave->number_of_days_requested)
                    ->salutation('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
