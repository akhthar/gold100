<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\AttendanceRequest;
use \DateTime;

class NewAttendanceRequest extends Notification
{
    use Queueable;

    public $attendanceRequest;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AttendanceRequest $attendanceRequest)
    {
        $this->attendanceRequest = $attendanceRequest;
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('dashboard/attendance');
        return (new MailMessage)
                    ->greeting('Hello ' . $this->attendanceRequest->employee->supervisor->name)
                    ->line('A new attendance request has been submitted that requires your attention.')
                    ->line('Employee: ' . $this->attendanceRequest->employee->name)
                    ->line('Event: ' . $this->attendanceRequest->event)
                    ->line('Time: ' . (new DateTime($this->attendanceRequest->time))->format('d-m-Y H:i:s'))
                    
                    // ->line('Number of Days Requested: ' . $this->leave->number_of_days_requested)
                  //  ->line('Requested By: ', $this->leave->requestedBy->name)
                  //  ->line('Requested On: ', (new DateTime($this->leave->created_at))->format('d-m-Y H:i:s'))
                    ->action('USE THIS LINK TO TAKE ACTION', $url)
                    ->salutation('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
