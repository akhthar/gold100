<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Company;
use \DateTime;

class Gold100InformationFormSubmission extends Notification
{
    use Queueable;

    public $company;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
        // $this->recipientName = $recipientName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // echo '<pre>';
        // print_r($this->company->company_name);
        // die();
        // $url = url('admin/requests');
        return (new MailMessage)
                    ->subject('Gold100 Information Form Submission')
                    ->greeting('Hello ' . $this->company->company_name)
                    ->line("You are receiving this email because '" . $this->company->email . "' was used to register for GOLD100.")

                    ->line('Company: ' . $this->company->company_name)
                    ->line('Form Submitted On: ' . (new DateTime($this->company->created_at))->format('d-m-Y H:i:s'))
                    ->line('')
                    ->line('Your information form has been submitted successfully. Our staff will contact you on further instructions.')
                  //  ->line('Requested By: ', $this->leave->requestedBy->name)
                  //  ->line('Requested On: ', (new DateTime($this->leave->created_at))->format('d-m-Y H:i:s'))
                    // ->action('USE THIS LINK TO TAKE ACTION', $url)
                    ->salutation('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
